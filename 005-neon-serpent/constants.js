const PLAYER_COLORS = [
    'rgba(240, 122, 172, 1)',
    'rgba(77, 195, 195 , 1)',
    'rgba(180, 167, 214, 1)',
    'rgba(255, 123, 113 , 1)',
    'rgba(93, 179, 116, 1)',
    'rgba(251, 229, 84, 1)',
    'rgba(255, 255, 255, 1)',
    'rgba(40, 40, 40, 1)',
];

const COLOR_FRUIT = 'rgba(255, 255, 255, 1)';
const COLOR_BOMB = 'rgba(0, 0, 0, 1)';

const COLOR_BACKGROUND = 'rgba(72, 73, 104, 1)';
const COLOR_BACKGROUND2 = 'rgba(35, 95, 145, 1)';

const COLOR_FOREGROUND = 'rgba(255, 255, 255, .5)';
const COLOR_SHADOW = 'rgba(60, 53, 78, .6)';
const SHADOW_OFFSET = 7;

const BLOCK_SIZE = 20;
const BLOCK_COLOR = "white";

const GLOW_INTENSITY = 12;

const FONT_POINTS = "20px Verdana";
const FONT = "GAME_FONT"
const GAME_FONT = new FontFace('GAME_FONT', "url(./styles/VanillaExtract.ttf)");

GAME_FONT.load().then(function(font){
    document.fonts.add(font);
});

const RESOLUTION = 1080;
const ASPECTRATIO = (16/9);
const CANVAS_WIDTH = RESOLUTION * ASPECTRATIO;
const CANVAS_HEIGHT = RESOLUTION;

const GUI_RADIUS = RESOLUTION / 6;
const GUI_COLOR = "white";

const PLAYER_RADIUS = RESOLUTION / 250;
const TAIL_WIDTH = 2 * PLAYER_RADIUS;
const DIRECTION_INDICATOR_LENGTH = PLAYER_RADIUS;

const FRUIT_RADIUS = 35;
const FRUIT_COLLISION_RADIUS = FRUIT_RADIUS;
const FRUIT_BORDER = 100;

const WINNING_SCORE = 10;

const SPEED = 1;
const SPEED_INCREASE = .001;
const BOOST = 1.9;
const BREAK = .55;
const STEER_SPEED = 2.2;
const TAIL_FRAME_CALC = 5;
const START_LENGTH = 10;
const LENGTH_INCREASE = 10;
const INCREASE_LENGTH_VALUE = START_LENGTH;

const EFFECT_DURATION_POP = 90;
const EFFECT_RADIUS = 30;

const EXPLODE_EFFECT_DURATION = 150;

const MAX_BULLET_TIME = 150;
const BULLET_SPEED = 3 * SPEED;
const BULLET_SIZE = PLAYER_RADIUS;

const ANALOG_THRESHOLD = .1;

const SPECIAL_RADIUS = 60;
const SPECIAL_COLLISION_RADIUS = SPECIAL_RADIUS;
const SPECIAL_PLACEMENT_THRESHOLD = .8;
const SPECIAL_DURATION = 4000;
const ANNOUNCMENT_TIME = 450;

const PLAYERITEM_PLACEMENT_THRESHOLD = 0;
const PLAYERITEM_COLLISION_RADIUS = SPECIAL_COLLISION_RADIUS;
const TRIGGERED_PLAYERITEM_COLLISION_RADIUS = FRUIT_COLLISION_RADIUS;
const TRIGGERED_PLAYERITEM_ACTIVATION_TIME = 100;

const END_OF_ROUND_PAUSE = 2500;

const HOLE_THRESHOLD = .2;
const HOLE_LENGTH = 3;
const HOLE_MIN_DIST = HOLE_LENGTH * 3.14;

const DISTORTION_STEERING = 7;
const DISTORTION_OFFSET = 10;

const SPECIAL_ROTATION_ANGLE = 1.62;

const NUMBER_OF_BOMBS_PLACED = 3;

const PACMAN_SPEED = .6 * SPEED;
const PACMAN_INITIAL_HEALTH = 5;

const TRIGGERED_PLAYER_ITEM_RADIUS = 10;

const MAX_BLOCK_HEALTH = 2;

const PACMAN_SIZE = 70;