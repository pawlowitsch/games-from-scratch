// gamepad controls
const connectedGamepads = [];
window.addEventListener("gamepadconnected", (event) => {
    connectedGamepads[event.gamepad.index] = event.gamepad
});

window.addEventListener("gamepaddisconnected", (event) => {
    delete connectedGamepads[event.gamepad.index];
});

setInterval(() => {
    let gamepads = navigator.getGamepads(); 
    for (let index = 0; index < gamepads.length; index++) {

        let gamepad = gamepads[index];
        if (!gamepad) continue;

        if (gamepad.buttons.filter(b => b.pressed).length > 0) {
            Controller.registerController("gamepad", index ,gamepad.id);
        }

        let playerId = Controller.getplayerIndexByController("gamepad", index);
        let playerControls = controls[playerId];

        if (!playerControls) continue;

        for (let buttonId in gamepad.buttons) {
            let button = gamepad.buttons[buttonId];

            switch (true) {
                case buttonId === "12": playerControls.up = button.pressed; break;
                case buttonId === "13": playerControls.down = button.pressed; break;
                case buttonId === "14": playerControls.left = button.pressed; break;
                case buttonId === "15": playerControls.right = button.pressed; break;

                case buttonId === "0": playerControls.south = button.pressed; break
                case buttonId === "1": playerControls.east = button.pressed; break
                case buttonId === "2": playerControls.west = button.pressed; break;
                case buttonId === "3": playerControls.north = button.pressed; break;
                case buttonId === "4": playerControls.l1 = button.pressed; break;
                case buttonId === "6": playerControls.l2 = button.pressed; break;
                case buttonId === "5": playerControls.r1 = button.pressed; break;
                case buttonId === "7": playerControls.r2 = button.pressed; break;
                case buttonId === "8": playerControls.select = button.pressed; break;
                case buttonId === "9": playerControls.start = button.pressed; break;
            }
        }

        let dx = 0;
        let dy = 0;

        if (gamepad.axes?.length > 0) {
            dx = gamepad.axes[0];
            dy = gamepad.axes[1];
        }

        playerControls.dx = 1;
        playerControls.dy = 1;

        if (Math.abs(dx)>ANALOG_THRESHOLD || Math.abs(dy)>ANALOG_THRESHOLD) {
            playerControls.up = (dy <= -ANALOG_THRESHOLD);
            playerControls.down = (dy <= -ANALOG_THRESHOLD)
            playerControls.left = (dx <= -ANALOG_THRESHOLD);
            playerControls.right = (dx >= ANALOG_THRESHOLD);

            playerControls.dx = dx;
            playerControls.dy = dy;
        }
    }
}, 1);
