
function __getKeyboardIdx(e) {
    let keyboardIdx = null;

    switch (e.keyCode) {
        // Keyboard 0
        case 38:
        case 40:
        case 37:
        case 39:
        case 190:
        case 188:
        case 13:
        case 77:
        case 80:
            keyboardIdx = 0;
            break;

        // Keyboard 0
        case 87:
        case 83:
        case 65:
        case 68:
        case 49:
        case 81:
        case 27:
        case 84:
        case 71:
            keyboardIdx = 1;
            break;
    }

    return keyboardIdx;
}

window.addEventListener("keydown", (e) => {
    const keyboardIdx = __getKeyboardIdx(e);

    if (keyboardIdx === null) {
        return;
    }

    Controller.registerController("keyboard", keyboardIdx, 0);
    let playerIndex = Controller.getplayerIndexByController("keyboard", keyboardIdx);

    switch (e.keyCode) {
        // Keyboard 0
        case 38: controls[playerIndex].south  = true; break;  //ArrowUp
        case 40: controls[playerIndex].west   = true; break;  //ArrowDown
        case 37: controls[playerIndex].left   = true; break;  //ArrowLeft
        case 39: controls[playerIndex].right  = true; break;  //ArrowRight
        
        case 190: controls[playerIndex].r2     = true; break;  //.
        case 188: controls[playerIndex].l2     = true; break;  //,
        
        case 13: controls[playerIndex].start  = true; break;  //ENTER
        case 77: controls[playerIndex].select = true; break;  //M 
        case 80: controls[playerIndex].north  = true; break;  //P
        
        // Keyboard 1
        case 87: controls[playerIndex].south  = true; break;  //W
        case 83: controls[playerIndex].west   = true; break;  //S
        case 65: controls[playerIndex].left   = true; break;  //A
        case 68: controls[playerIndex].right  = true; break;  //D

        case 81: controls[playerIndex].r2     = true; break;  //Q
        case 49: controls[playerIndex].l2     = true; break;  //1

        case 27: controls[playerIndex].start  = true; break;  //ESC
        case 84: controls[playerIndex].select = true; break;  //T 
        case 71: controls[playerIndex].north  = true; break;  //G
        
    }
});

window.addEventListener("keyup", (e) => {
    const keyboardIdx = __getKeyboardIdx(e);

    if (keyboardIdx === null) {
        return;
    }

    Controller.registerController("keyboard", keyboardIdx, 0);
    let playerIndex = Controller.getplayerIndexByController("keyboard", keyboardIdx);

    switch (e.keyCode) {
        // Keyboard 0
        case 38: controls[playerIndex].south  = false; break;  //ArrowUp
        case 40: controls[playerIndex].west   = false; break;  //ArrowDown
        case 37: controls[playerIndex].left   = false; break;  //ArrowLeft
        case 39: controls[playerIndex].right  = false; break;  //ArrowRight
        
        case 190: controls[playerIndex].r2     = false; break;  //.
        case 188: controls[playerIndex].l2     = false; break;  //,
        
        case 13: controls[playerIndex].start  = false; break;  //ENTER
        case 77: controls[playerIndex].select = false; break;  //M 
        case 80: controls[playerIndex].north  = false; break;  //P
        
        // Keyboard 1
        case 87: controls[playerIndex].south  = false; break;  //W
        case 83: controls[playerIndex].west   = false; break;  //S
        case 65: controls[playerIndex].left   = false; break;  //A
        case 68: controls[playerIndex].right  = false; break;  //D

        case 81: controls[playerIndex].r2     = false; break;  //Q
        case 49: controls[playerIndex].l2     = false; break;  //1

        case 27: controls[playerIndex].start  = false; break;  //ESC
        case 84: controls[playerIndex].select = false; break;  //T 
        case 71: controls[playerIndex].north  = false; break;  //G
    }
});