function xy(x) {
    o = document.getElementById(x);
    var l =o.offsetLeft; var t = o.offsetTop;
    while (o=o.offsetParent)
        l += o.offsetLeft;
    o = document.getElementById(x);
    while (o=o.offsetParent)
        t += o.offsetTop;
    return [l,t];
}
    

if (isMobile()) {
    const headHtml = `
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <style>
        body {margin: 0}
        canvas {width: 100%}
    </style>
    `;
    document.querySelector("head").innerHTML = headHtml;

    const analogSize = 170;
    const buttonSize = 60;
    const controllerHtml = `
        <style>
            body {
                overflow: hidden;
            }
            #vcontroller,
            #vcontroller * {
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .button {
                height: ${buttonSize}px;
                width: ${buttonSize}px;
                border-radius: ${buttonSize/2}px;
                background: white;
                text-align: center;
                line-height: ${buttonSize}px;
                position: absolute;
                cursor: pointer;
            }

            .button.pressed {
                color: white;
                background: grey;
            }

            #vcontroller {
                position: fixed;
                bottom: 0;
    
                height: 40vh;
                width: 95vw;
                margin: auto;
            }

            #buttonWest {
                left: 61%;
                top: 50%;
            }
            #buttonEast {
                left: 85%;
                top: 50%;
            }
            #buttonNorth {
                left: 73%;
                top: 30%;
            }
            #buttonSouth {
                left: 73%;
                top: 70%;
            }
            #buttonStart,
            #buttonL1,
            #buttonR1 {
                left: 50%;
                top: 80%;
                height: ${buttonSize/2}px;
                line-height: ${buttonSize/2}px;
            }
            #buttonL1 {
                left: 5%;
                top: 10%;
            }
            #buttonR1 {
                left: 85%;
                top: 10%;
            }
            #analogStick {
                position: absolute;
                height: ${analogSize}px;
                width: ${analogSize}px;
                border-radius: ${analogSize/2}px;
                background: white;
                top: 30%;
                left: 5%;
                cursor: pointer;
            }
            #stick {
                pointer-events: none;
                position: absolute;
                background: black;
                height: 20px;
                width: 20px;
                border-radius: 10px;
                top: calc(50% - 10px);
                left: calc(50% - 10px);
            }
        </style>
        <div id="analogStick"><div id="stick"></div></div>

        <div class="button" id="buttonR1">R1</div>
        <div class="button" id="buttonL1">L1</div>
        <div class="button" id="buttonSouth">S</div>
        <div class="button" id="buttonWest">W</div>
        <div class="button" id="buttonNorth">N</div>
        <div class="button" id="buttonEast">E</div>
        <div class="button" id="buttonStart">Start</div>
    `;
    const vcEl = document.querySelector("#vcontroller");
    vcEl.innerHTML = controllerHtml;

    const stick = document.querySelector("#stick");
    const buttons = {};

    const onDown = (e) => {
        try {
            e.preventDefault();
            e.stopImmediatePropagation();
        } catch (err) {}
        
        Controller.registerController("onscreen", 0, 0);

        const el = e.target ? e.target : e.path.shift();
        const id = el.id;

        buttons[id] = true;

        const playerIndex = Controller.getplayerIndexByController("onscreen", 0);
        
        if (id === "buttonSouth") controls[0].south = true;
        if (id === "buttonNorth") controls[0].north = true;
        if (id === "buttonWest") controls[0].west = true;
        if (id === "buttonEast") controls[0].east = true;
        if (id === "buttonStart") controls[0].start = true;
        if (id === "buttonR1") controls[0].r1 = true;
        if (id === "buttonL1") controls[0].l1 = true;

        el.classList.add("pressed");
    };
    const onMove = (e) => {
        try {
            e.preventDefault();
            e.stopImmediatePropagation();
        } catch (err) {}

        const el = e.target ? e.target : e.path.shift();
        const id = el.id;

        if (id === "analogStick" && buttons[id] === true) {
            let dx = (e.offsetX - (analogSize/2)) / (analogSize/2);
            let dy = (e.offsetY - (analogSize/2)) / (analogSize/2);

            let offsetX, offsetY;
            if (e.touches) {
                const [x, y] = xy("analogStick");

                let touch = e.touches[0];
                if (e.touches.length>1 && e.touches[1].clientX < touch.clientX) {
                    touch = e.touches[1];
                }

                offsetX = (touch.clientX - x);
                offsetY = (touch.clientY - y);

                dx = Math.min(1, Math.max(-1, (offsetX - (analogSize/2)) / (analogSize/2)));
                dy = Math.min(1, Math.max(-1, (offsetY - (analogSize/2)) / (analogSize/2)));
            }

            controls[0].up = (dy <= -ANALOG_THRESHOLD);
            controls[0].down = (dy <= -ANALOG_THRESHOLD)
            controls[0].left = (dx <= -ANALOG_THRESHOLD);
            controls[0].right = (dx >= ANALOG_THRESHOLD);

            controls[0].dx = dx;
            controls[0].dy = dy;

            stick.style.top = `${offsetY - 10}px`;
            stick.style.left = `${offsetX - 10}px`;
        }
    };
    const onUp = (e) => {
        try {
            e.preventDefault();
            e.stopImmediatePropagation();
        } catch (err) {}
        
        const el = e.target ? e.target : e.path.shift();
        const id = el.id;

        buttons[id] = false;
        
        if (id === "buttonSouth") controls[0].south = false;
        if (id === "buttonNorth") controls[0].north = false;
        if (id === "buttonWest") controls[0].west = false;
        if (id === "buttonEast") controls[0].east = false;
        if (id === "buttonStart") controls[0].start = false;
        if (id === "buttonR1") controls[0].r1 = false;
        if (id === "buttonL1") controls[0].l1 = false;
        
        if (id === "analogStick") controls[0].up = false;
        if (id === "analogStick") controls[0].down = false;
        if (id === "analogStick") controls[0].left = false;
        if (id === "analogStick") controls[0].right = false;
        if (id === "analogStick") controls[0].dx = 1;
        if (id === "analogStick") controls[0].dy = 1;

        el.classList.remove("pressed");

        stick.style.top = `${(analogSize/2) - 10}px`;
        stick.style.left = `${(analogSize/2) - 10}px`;
    };

    document.querySelectorAll("#analogStick, .button").forEach(e => {
        e.addEventListener("touchstart", onDown, { passive: false });
        e.addEventListener("touchmove", onMove,  { passive: false });
        e.addEventListener("touchend", onUp,  { passive: false });
        e.addEventListener("touchcancel", onUp,  { passive: false });
    });
}