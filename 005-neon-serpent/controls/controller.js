class Controller {
    // d-pad
    up = false;
    down = false;
    left = false;
    right = false;

    // action buttons
    south = false;
    east = false;
    west = false;
    north = false;

    // shoulder
    l1 = false;
    l2 = false;
    r1 = false;
    r2 = false;

    // meta
    select = false;
    start = false;

    // left analog stick 
    dx = 1;
    dy = 1;

    static buttonCache = [];

    // "gamepad-IDX": playerIndex
    // "keyboard-IDX": playerIndex
    // "onscreen" => playerIndex
    static playerControllerLinks = {};
    static playerGamepadVendor = {};

    static resetLinks() {
        Controller.playerControllerLinks = {};
    }

    static registerController(controllerType, controllerIdx, gampadId) {
        const controllerIdentifier = `${controllerType}-${controllerIdx}`;

        if (Controller.getplayerIndexByController(controllerType, controllerIdx) === null) {
            if(currentScene instanceof MainScene ||currentScene instanceof SelectColorScreen){
                Controller.playerControllerLinks[controllerIdentifier] = players.length;
                Controller.playerGamepadVendor[gampadId] = players.length;
                addPlayer();
                addController();            
            }
        }
    }

    static getControllerIdentifierByplayerIndex(playerIndex) {
        let controllerIdentifier = Object.keys(Controller.playerControllerLinks).find(key => Controller.playerControllerLinks[key] == playerIndex);
        
        return controllerIdentifier;
    }
    
    static getGampadVendorByplayerIndex(playerIndex){
        let gamepadVandor = Object.keys(Controller.playerGamepadVendor).find(key => Controller.playerGamepadVendor[key] == playerIndex);
        return gamepadVandor;
    }

    static getplayerIndexByController(controllerType, controllerIdx) {
        const controllerIdentifier = `${controllerType}-${controllerIdx}`;
        const playerIndex = Controller.playerControllerLinks[controllerIdentifier];

        if (typeof playerIndex === "undefined") return null;

        return playerIndex;
    }

    /**
     * A single press of a button is constructed of 3 states.
     * 
     * NOT-PRESSED, PRESSED
     * 
     * The previous implementation was only considering 1 states
     * 
     * PRESSED
     * 
     * This implemenation resultet in automatically pressing the start button
     * on a scene switch
     * 
     * @param {*} cacheKey 
     * @param {*} input 
     * @param {*} callback 
     */
    static singlePress(cacheKey, input, callback) {
        if (!Array.isArray(input)) {
            input = [input];
        }

        for (const playerIndex in players) {
            const player = players[playerIndex];
            const controller = controls[playerIndex];

            if (!player) continue;
            if (!controller) continue;

            if (!Controller.buttonCache[playerIndex]) {
                Controller.buttonCache[playerIndex] = {};
            }

            const cache = Controller.buttonCache[playerIndex];

            for (let key of input) {
                if (cache[cacheKey] === undefined) cache[cacheKey] = [];

                // check if cache for key is empty
                // if so, add current controller value for that key in cache
                if (cache[cacheKey].length === 0) {
                    cache[cacheKey].push( controller[key] );
                }

                // check if last cache item has same value as
                // the last that we stored.
                // if so, continue, as there was no change
                if (
                    cache[cacheKey][cache[cacheKey].length-1] === controller[key]
                ) {
                    continue;
                }

                // push the net value to the cache
                cache[cacheKey].push(controller[key]);

                // check if we now have the correct value pattern (false, true) at the end
                if (cache[cacheKey].length>=2) {
                    const l = cache[cacheKey].length;
                    if (
                        cache[cacheKey][l-1] === true &&
                        cache[cacheKey][l-2] === false
                    ) {
                        // reset cache
                        cache[cacheKey] = [ ];
                        callback(playerIndex, player, controller);
                    }
                }
            }
        }
    }

    static allPlayerPressed(cacheKey, input, callback) {
        if (!Array.isArray(input)) {
            input = [input];
        }

        let allConfirmed = true;

        for (const playerIndex in players) {
            const player = players[playerIndex];
            const controller = controls[playerIndex];

            if (!player) continue;
            if (!controller) continue;

            if (!Controller.buttonCache[playerIndex]) {
                Controller.buttonCache[playerIndex] = {};
            }

            const cache = Controller.buttonCache[playerIndex];

            for (let key of input) {
                if (!cache[cacheKey] && controller[key] === true) {
                    cache[cacheKey] = true;
                }
            }

            allConfirmed &= cache[cacheKey];
        }

        if (allConfirmed) {
            callback();
        }
    }
}