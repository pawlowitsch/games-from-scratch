class SelectColorScreen {
    numPlayer = 2;
    selection = 0;
    startPressed = false;

    paused = false;

    pressedButtons = [];

    layers = {
        paused: null,
    };

    visualEffects = [];

    frameCtr = 0;
    drawCtr = 0;

    imageControlls = null;
    imageInstructions = null;
    imageCheck = null;
    imageButton = null;

    constructor() {
        this.imageControlls = new Image();
        this.imageControlls.src = "./graphics/controlls.svg";

        this.imageInstructions = new Image();
        this.imageInstructions.src = "./graphics/ready_up.svg";

        this.imageCheck = new Image();
        this.imageCheck.src = "./graphics/check.svg";
    }

    audioFiles = {
    };

    // construct  image to show 
    getButtonImage(vendor, button) {
        this.imageButton = new Image();
        this.imageButton.src = `./graphics/gamepad/${vendor}_${button}.svg`;
    };

    wrightText(text, x, y) {
        context.save();
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.fillStyle = 'rgba(255, 255, 255, 1)';
        context.font = "bold 15px GAME_FONT";
        context.textAlign = "left";
        context.font = "18px GAME_FONT";
        context.fillText(
            text,
            x,
            y,
        );
        context.restore();
    }

    drawButton(vendor, button, x, y, targetImageSize){
        context.save();
        this.getButtonImage(vendor, button)
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.beginPath();
        context.drawImage(
            this.imageButton,
            0, 0,
            this.imageButton.width, this.imageButton.height,
            x - (targetImageSize / 2), y - (targetImageSize),
            targetImageSize, targetImageSize
        );
        context.closePath();
        context.restore();
    }

    reset() { }

    init() {
        //this.layers.intro = new IntroLayer();
        //this.layers.frame = new FrameLayer();
        this.layers.background = new AnimatedBackground();
        this.layers.frame = new FrameLayer();
        this.layers.paused = new PausedLayer();
    }

    /*drawInstructions() {
        context.save();
        context.beginPath();
        if (this.imageInstructions) {
            let ratio = this.imageInstructions.width / this.imageInstructions.height;
            let targetImageSize = 38;
            context.drawImage(
                this.imageInstructions,
                0, 0,
                this.imageInstructions.width, this.imageInstructions.height,
                (CANVAS_WIDTH / 2) - (targetImageSize * ratio / 2), 170 - (targetImageSize / 2),
                targetImageSize * ratio, targetImageSize
            );
            context.closePath();
            context.restore();
        }
    }*/

    drawControlls() {
        context.save();
        context.beginPath();
        if (this.imageControlls) {
            let ratio = this.imageControlls.width / this.imageControlls.height;
            let targetImageSize = 200;
            context.drawImage(
                this.imageControlls,
                0, 0,
                this.imageControlls.width, this.imageControlls.height,
                150, (CANVAS_WIDTH - 300),
                targetImageSize * ratio, targetImageSize
            );
            context.closePath();
            context.restore();
        }
        context.restore();
    }

    drawColors(context, drawCtr, scene) {
        const border = 50;
        const space = (CANVAS_WIDTH - border - border) / players.length;

        for (let playerIndex in players) {
            if (!this.pressedButtons[playerIndex]) continue;
            
            const controller = controls[playerIndex];
            const player = Player.clonePlayer(players[playerIndex]);
            let dx = border + (space / 2) + (space * playerIndex);
            let px = border + (space / 2) + (space * playerIndex) - 5;
            let y = 280;

            
            // draw player name
            context.save();
            context.shadowOffsetY = null;
            context.shadowColor = null;
            context.textAlign = "center";
            context.font = "30px GAME_FONT";
            if (controller.left && !this.pressedButtons[playerIndex].confirmed) {
                dx = dx - 4; 
                px = px - 4; 
                context.font = "35px GAME_FONT";
                context.shadowOffsetY = SHADOW_OFFSET;
                context.shadowColor = COLOR_SHADOW;
            }
            if (controller.right && !this.pressedButtons[playerIndex].confirmed) {
                dx = dx + 4; 
                px = px + 4; 
                context.font = "35px GAME_FONT";
                context.shadowOffsetY = SHADOW_OFFSET;
                context.shadowColor = COLOR_SHADOW;
            }
            context.fillStyle = 'rgba(255, 255, 255, 1)';
            context.fillText(
                `Player${1 * playerIndex + 1}`,
                dx,
                y
            );
            context.restore();

            // draw player tail
            let top = 310;
            player.alive = true;
            player.head = {
                x: dx,
                y: top,
            }
            player.tail = [];
            let tailPoints = 100;
            let maxModX = 5;
            let dy = 1;

            for (let i = 0; i < tailPoints; i++) {
                let modX = maxModX * Math.sin(
                    (Math.PI * 8 * i / tailPoints) +
                    (.05 * drawCtr)
                );
                player.tail.push(
                    { x: dx + modX, y: top + dy * (i + 2) }
                );
            }
            player.head = player.tail[0];
            player.drawOnlyPlayer = true;
            player.color = player.tailColor = PLAYER_COLORS[this.pressedButtons[playerIndex].colorIdx];
            if (this.pressedButtons[playerIndex].dashed) {
                player.drawDashed = true;
            }
            player.draw(context, drawCtr, scene);

            // draw input icon
            let controllerIcon = new Path2D(`m ${px - 10}, 409 a 2.860686,2.860686 0 0 0 -2.21861,2.35886 l -3.70507,24.01867 a 2.3067938,2.3067938 0 0 0 1.73388,2.59299 l 6.82178,1.66195 a 3.0242795,3.0242795 45 0 0 3.23123,-1.2598 l 3.5018,-5.24759 a 4.6660697,4.6660697 0 0 1 3.27357,-2.03673 l 14.02776,-1.84244 a 4.5477456,4.5477456 45 0 1 3.66087,1.15288 l 4.55756,4.16617 a 2.8305882,2.8305882 0 0 0 3.37386,0.33361 l 6.60721,-3.99224 a 2.303184,2.303184 0 0 0 0.87338,-2.99213 l -10.84663,-21.93773 a 2.9173955,2.9173955 0 0 0 -3.03327,-1.59401 l -31.65999,4.58141 a 2.860686,2.860686 0 0 0 -0.19933,0.0361 z m 26.84293,0.32755 a 2.4365702,2.4365702 0 0 1 0.54226,-0.061 2.4365702,2.4365702 0 0 1 2.43635,2.43641 2.4365702,2.4365702 0 0 1 -2.43632,2.43686 2.4365702,2.4365702 0 0 1 -2.43695,-2.43682 2.4365702,2.4365702 0 0 1 1.89466,-2.37541 z m -21.88267,4.81903 2.90006,-0.4277 0.55852,3.78885 3.97614,-0.58606 0.42771,2.90006 -3.97665,0.58616 0.5452,3.69618 -2.90016,0.4272 -0.5451,-3.69568 -3.50849,0.51705 -0.4272,-2.90015 3.50849,-0.51705 z m 17.52022,-0.43996 a 2.4365702,2.4365702 0 0 1 0.54226,-0.061 2.4365702,2.4365702 0 0 1 2.43645,2.43693 2.4365702,2.4365702 0 0 1 -2.43642,2.43635 2.4365702,2.4365702 0 0 1 -2.43686,-2.43632 2.4365702,2.4365702 0 0 1 1.89457,-2.37591 z m 8.84943,-0.1885 a 2.4365702,2.4365702 0 0 1 0.54225,-0.061 2.4365702,2.4365702 0 0 1 2.43645,2.43692 2.4365702,2.4365702 0 0 1 -2.43692,2.43645 2.4365702,2.4365702 0 0 1 -2.43635,-2.43641 2.4365702,2.4365702 0 0 1 1.89457,-2.37592 z m -4.4604,4.47018 a 2.4365702,2.4365702 0 0 1 0.54226,-0.061 2.4365702,2.4365702 0 0 1 2.43695,2.43683 2.4365702,2.4365702 0 0 1 -2.43692,2.43645 2.4365702,2.4365702 0 0 1 -2.43635,-2.43642 2.4365702,2.4365702 0 0 1 1.89406,-2.37581 z`);
            let keyboardIcon = new Path2D(`m ${px + 14}, 409 -8.33513,2.53868 -1.61353,0.49143 -25.99555,7.91763 c -1.21064,0.36873 -1.88872,1.64 -1.51998,2.85065 l 7.23269,23.74679 c 0.36873,1.21063 1.6405,1.88857 2.85114,1.51983 l 49.54558,-15.0904 c 1.21064,-0.36874 1.88857,-1.6405 1.51984,-2.85115 l -7.2327,-23.74676 c -0.36873,-1.21064 -1.64036,-1.88809 -2.85098,-1.51936 l -11.68877,3.56011 z m 7.94391,2.87836 3.60969,-1.09944 c 0.42736,-0.13011 0.89336,0.16622 1.04506,0.66433 l 0.84257,2.76635 c 0.15172,0.49809 -0.07,1.00393 -0.4973,1.13409 l -3.60971,1.09943 c -0.42733,0.13011 -0.89332,-0.16622 -1.04504,-0.66433 l -0.84256,-2.76634 c -0.15172,-0.4981 0.07,-1.00394 0.49729,-1.13409 z m -30.26075,9.33013 3.6097,-1.09942 c 0.42734,-0.13012 0.89333,0.16622 1.04505,0.66434 l 0.84256,2.76634 c 0.15171,0.49809 -0.07,1.00393 -0.49731,1.13409 l -3.60968,1.09943 c -0.42735,0.13012 -0.89385,-0.16606 -1.04555,-0.66417 l -0.84257,-2.76635 c -0.15171,-0.4981 0.0705,-1.00409 0.4978,-1.13426 z m -7.82425,2.41226 3.60969,-1.09943 c 0.42734,-0.13011 0.89334,0.16622 1.04505,0.66433 l 0.84256,2.76634 c 0.15172,0.4981 -0.07,1.00395 -0.49729,1.1341 l -3.60968,1.09943 c -0.42736,0.13022 -0.89385,-0.16607 -1.04555,-0.66418 l -0.84257,-2.76635 c -0.15172,-0.4981 0.0705,-1.00407 0.49779,-1.13424 z m 22.7732,-6.92807 3.6097,-1.09942 c 0.42734,-0.13011 0.89383,0.16606 1.04554,0.66417 l 0.84256,2.76635 c 0.15171,0.4981 -0.0705,1.00407 -0.49778,1.13424 l -3.60971,1.09943 c -0.42734,0.13011 -0.89334,-0.16622 -1.04504,-0.66433 l -0.84257,-2.76635 c -0.15171,-0.4981 0.07,-1.00393 0.4973,-1.13409 z m -7.62083,2.39729 3.60969,-1.09943 c 0.42736,-0.13012 0.89337,0.16623 1.04507,0.66434 l 0.84256,2.76633 c 0.15171,0.49811 -0.07,1.00393 -0.4973,1.1341 l -3.6097,1.09943 c -0.42733,0.13011 -0.89385,-0.16607 -1.04554,-0.66418 l -0.84257,-2.76635 c -0.15172,-0.49809 0.0705,-1.00408 0.49779,-1.13424 z m 15.16712,-4.54446 3.6097,-1.09942 c 0.42735,-0.13012 0.89335,0.16622 1.04506,0.66433 l 0.84257,2.76634 c 0.1517,0.4981 -0.07,1.00393 -0.49731,1.13409 l -3.6097,1.09942 c -0.42734,0.13012 -0.89384,-0.16606 -1.04555,-0.66415 l -0.84256,-2.76636 c -0.15172,-0.49809 0.0705,-1.00408 0.49779,-1.13425 z m -9.68709,10.13029 3.60969,-1.09944 c 0.42734,-0.13009 0.89335,0.16623 1.04506,0.66434 l 0.84255,2.76635 c 0.15173,0.49809 -0.07,1.00393 -0.49728,1.13409 l -3.60971,1.09943 c -0.42735,0.13012 -0.89382,-0.16606 -1.04554,-0.66419 l -0.84256,-2.76634 c -0.15172,-0.4981 0.0705,-1.00408 0.49779,-1.13424 z m 15.66177,-4.65675 6.8605,-2.08956 c 0.42734,-0.13011 0.89334,0.16623 1.04505,0.66434 l 0.89525,2.93936 c 0.15172,0.49811 -0.0701,1.00344 -0.49744,1.1336 l -6.86049,2.08954 c -0.42735,0.13012 -0.8932,-0.16571 -1.04492,-0.66383 l -0.89524,-2.93937 c -0.15172,-0.4981 0.07,-1.00392 0.49729,-1.13408 z m -22.59762,7.07176 3.61018,-1.09959 c 0.42735,-0.13011 0.8932,0.16571 1.0449,0.66384 l 0.84272,2.76685 c 0.15172,0.49809 -0.0701,1.00344 -0.49745,1.13359 l -3.61019,1.09958 c -0.42735,0.13011 -0.8932,-0.1657 -1.0449,-0.66382 l -0.84272,-2.76686 c -0.15172,-0.49809 0.0701,-1.00343 0.49746,-1.13359 z m -11.70115,3.64006 7.41317,-2.25788 c 0.42735,-0.13012 0.89371,0.16555 1.04541,0.66368 l 0.80055,2.62843 c 0.15171,0.49809 -0.0705,1.00408 -0.49779,1.13425 l -7.41318,2.25787 c -0.42724,0.1301 -0.89382,-0.16606 -1.04553,-0.66419 l -0.80056,-2.62841 c -0.15171,-0.49811 0.0707,-1.00362 0.49793,-1.13375 z m 26.42975,-8.01206 3.60968,-1.09942 c 0.42735,-0.13022 0.89369,0.16555 1.0454,0.66368 l 0.84256,2.76634 c 0.15172,0.4981 -0.0705,1.00409 -0.49779,1.13424 l -3.60968,1.09944 c -0.42735,0.13012 -0.89336,-0.16622 -1.04507,-0.66434 l -0.84255,-2.76634 c -0.15171,-0.49809 0.0701,-1.00345 0.49745,-1.1336 z m -18.47373,12.91938 29.99477,-9.13569 c 0.42736,-0.13012 0.89389,0.16617 1.04556,0.66417 l 0.89525,2.93938 c 0.15171,0.49798 -0.0705,1.00408 -0.4978,1.13424 l -29.99477,9.13568 c -0.42733,0.13022 -0.89338,-0.16632 -1.04505,-0.66431 l -0.89526,-2.93938 c -0.1517,-0.49798 0.07,-1.00393 0.4973,-1.13409 z`);

            const inputIdentifier = Controller.getControllerIdentifierByplayerIndex(playerIndex);            
            let vendorId = Controller.getGampadVendorByplayerIndex(playerIndex);

            const inputNumber = inputIdentifier.split("-").pop();
            let inputIcon = controllerIcon;
            if (inputIdentifier.startsWith("keyboard-")) {
                inputIcon = keyboardIcon; 
                vendorId = "";
            }

            context.save();
            context.fillStyle = PLAYER_COLORS[this.pressedButtons[playerIndex].colorIdx];
            context.shadowOffsetY = SHADOW_OFFSET;
            context.shadowColor = COLOR_SHADOW;
            context.fill(inputIcon);
            context.restore();

            // draw input number
            context.textAlign = "center";
            context.font = "24px GAME_FONT";
            context.fillText(
                inputNumber,
                dx + 40,
                350,
            );

            // determ instruction keys by input type or gampad brand
            if (inputIdentifier.startsWith("keyboard-")) {
                vendorId = "";
            }
            try {
                if(vendorId.includes("XInput")){vendorId = "xinput"};
                if(vendorId.includes("054c")){vendorId = "dualshock"};
                if(vendorId.includes("057e")){vendorId = "nintendo"};
            } catch (e) {
                vendorId = "xinputcon";
            }
            
            // determinate keys
            let readyKey = "south"
            if(inputIdentifier === "keyboard-0"){readyKey="^"};
            if(inputIdentifier === "keyboard-1"){readyKey = "W"};
            
            let changeColorKey = "dpad"
            if(inputIdentifier === "keyboard-0"){changeColorKey = "<>"};
            if(inputIdentifier === "keyboard-1"){changeColorKey = "A-D"};
            
            let removePlyerKey = "east"
            if(inputIdentifier === "keyboard-0"){removePlyerKey="v"};
            if(inputIdentifier === "keyboard-1"){removePlyerKey = "S"};

            // draw instruction
            if (!this.pressedButtons[playerIndex].confirmed) {
                this.wrightText("Change Color", dx, 500);
                this.wrightText("Ready?", dx, 530);
                this.wrightText("Remove Player", dx, 560);
                if (inputIdentifier.startsWith("gamepad-")) {
                    let bx=dx-20;
                    let dy = 5;
                    let size = 25;
                    this.drawButton(vendorId, changeColorKey, bx, 500+dy, size);
                    this.drawButton(vendorId, readyKey, bx, 530+dy, size);
                    this.drawButton(vendorId, removePlyerKey, bx, 560+dy, size);
                } else {
                    this.wrightText(changeColorKey, dx-40, 500);
                    this.wrightText(readyKey, dx-40, 530);
                    this.wrightText(removePlyerKey, dx-40, 560);
                }
                
            }

            // draw confirmation checkmark when confirmed
            if (this.pressedButtons[playerIndex].confirmed) {
                let targetImageSize = 40;
                context.beginPath();
                context.drawImage(
                    this.imageCheck,
                    0, 0,
                    this.imageCheck.width, this.imageCheck.height,
                    dx - (targetImageSize / 2), 550 - (targetImageSize),
                    targetImageSize, targetImageSize
                );
                context.closePath();
            }
        }
    }

    draw() {
        this.drawCtr++;

        this.layers.background.draw(context, this.frameCtr, this);
        this.layers.frame.draw(context, this.frameCtr, this);

        this.drawControlls();
        //this.drawInstructions();

        context.textAlign = 'center';
        context.font = "30px GAME_FONT";
        context.fillStyle = 'rgba(255, 255, 255, 1)'
        context.fillText(`SELECT YOUR COLOR:`, CANVAS_WIDTH / 2, 100,);


        this.drawColors(context, this.drawCtr, this);


        for (let visualEffect of this.visualEffects) {
            visualEffect.draw(context, this.frameCtr, this);
        }

        if (this.paused) {
            this.layers.paused.draw(context, this.frameCtr, this);
        }
        
        // draw possible colors
        for (let i = 0; i <= PLAYER_COLORS.length; i++) {
            const boarderX = 600;
            const maxX = CANVAS_WIDTH - boarderX;
            const minX = boarderX;

            let dx = (maxX-minX) / PLAYER_COLORS.length
            
            context.save();
            context.fillStyle = PLAYER_COLORS[i];
            context.shadowOffsetY = SHADOW_OFFSET;
            context.shadowColor = COLOR_SHADOW;
            context.beginPath();
            context.arc(
                minX+(dx*i),
                CANVAS_HEIGHT-200,
                30,
                0,
                2 * Math.PI,
                false
            );
            context.fill();
            context.restore();
        };
    }

    logic() {
        this.frameCtr++;

        this.visualEffects = this.visualEffects.filter(({ t }) => t < EFFECT_DURATION_POP);
        for (let visualEffect of this.visualEffects) {
            visualEffect.logic(this.frameCtr, this);
        }

        for (let playerIndex in players) {
            const player = players[playerIndex];
            const controller = controls[playerIndex];

            if (!this.pressedButtons[playerIndex]) {
                this.pressedButtons[playerIndex] = {
                    startPressed: false,
                    colorIdx: 1 * playerIndex,
                    selection: 0,
                    confirmed: false,
                    dashed: false,
                };
            }

            
            if (controller.enter || controller.south && this.paused === false) {this.pressedButtons[playerIndex].startPressed = true; SoundService.playSound("ching");}
            if ((!controller.enter && !controller.south) && this.pressedButtons[playerIndex].startPressed) {
                this.pressedButtons[playerIndex].confirmed = !this.pressedButtons[playerIndex].confirmed;
                this.pressedButtons[playerIndex].startPressed = false;
            }
            if (!this.pressedButtons[playerIndex].confirmed) {                
                if (controller.left) {this.pressedButtons[playerIndex].selection = -1; SoundService.playSound("ding");}
                if (controller.right) {this.pressedButtons[playerIndex].selection = 1; SoundService.playSound("ding");}

                if (!controller.left && !controller.right && this.pressedButtons[playerIndex].selection != 0) {
                    this.pressedButtons[playerIndex].colorIdx += this.pressedButtons[playerIndex].selection;
                    if (this.pressedButtons[playerIndex].colorIdx < 0 || this.pressedButtons[playerIndex].colorIdx >= PLAYER_COLORS.length) {
                        this.pressedButtons[playerIndex].dashed = !this.pressedButtons[playerIndex].dashed;
                    }
                    this.pressedButtons[playerIndex].colorIdx = (this.pressedButtons[playerIndex].colorIdx + PLAYER_COLORS.length) % PLAYER_COLORS.length;
                    this.pressedButtons[playerIndex].selection = 0;
                }
            }
        }

        const allPlayersConfirmed = this.pressedButtons.filter(v => v.confirmed).length === this.pressedButtons.length;
        if (allPlayersConfirmed) {
            for (let playerIndex in players) {
                const player = players[playerIndex];
                const color = PLAYER_COLORS[this.pressedButtons[playerIndex].colorIdx];
                player.color = player.tailColor = color;
                player.drawDashed = this.pressedButtons[playerIndex].dashed;
            }

            switchScene(GameScene);
        }

        Controller.singlePress("pause game", "start", (playerIndex, player, controller) => { 
            this.paused = !this.paused; 
        });

        if(this.paused === true) {
            Controller.singlePress("back to main screen", "north", (playerIndex, player, controller) => {
                switchScene(MainScene);
            });
    
            Controller.singlePress("fullscreen", "select", (playerIndex, player, controller) => {
                toggleFullscreen();
            });
        }
    }
}