class GameScene {
    paused = true;
    getReady = false;
    endOfGame = false;
    startOfRound = false;
    endOfRound = false;

    pressedButtons = [];

    gameSpeedIncrease = 0;
    maxLength = 0;

    visualEffects = [];
    playerItems = [];
    bullets = [];
    blocks = [];
    bombs = [];
    pacman = null;
    fruit = null;

    special = null;

    frameCtr = 0;

    collider = null;

    actions = {};

    /**
     * 
     * @param {*} name 
     * @param {*} fn 
     */
    registerAction(name, fn) {
        this.actions[name] = fn;
    }

    /**
     * 
     * @param {*} name 
     * @param {*} args 
     */
    callAction(name, args) {
        if (notset(this.actions[name])) throw new Error(`No such action ${name}`);
        if (notset(args)) args = [];
        
        this.actions[name](...args);
    }

    layers = {
        background: null,
        frame: null,
        startofround: null,
        endofround: null,
        getready: null,
        endofgame: null,
        hud: null,
        paused: null,
    };

    addVisualEffect(ve) {
        if (notset(ve) || !(ve instanceof VisualEffect)) throw new Error("Expected instance of VisualEffect");

        this.visualEffects.push(ve);
    };

    init() {
        this.layers.background = new AnimatedBackground();
        this.layers.frame = new FrameLayer();
        this.layers.getready = new GetReadyLayer();
        this.layers.endofgame = new endOfGameLayer();
        this.layers.startofround = new startOfRoundLayer();
        this.layers.endofround = new endOfRoundLayer();
        this.layers.endofround.onCountdownOverFn = this.reset.bind(this);
        this.layers.startofround.onCountdownOverFn = this.startGame.bind(this);

        this.layers.hud = new HUDLayer();
        this.layers.paused = new PausedLayer();

        this.collider = new Collider();

        registerColliderFunctionsToCollider(this.collider);
        registerActions(this);

        this.fruit = new Fruit(0, 0);
        
        this.reset();

        for (let playerIndex in players){
            const player = players[playerIndex];
            player.wins = 0;
        }
    }

    /**
     * Resets the instance variables to a definite state for a new round
     */
    reset() {
        Controller.buttonCache = [];

        const distanceToCenter = CANVAS_HEIGHT / 3;
        const degreeStep = 360 / players.length;

        this.gameSpeedIncrease = 0;
        this.currentSteerSpeed = STEER_SPEED;

        this.frameCtr = 0;

        this.special = null;

        if (this.pacman) {
            this.pacman.die(this);
        }
        this.pacman = null;

        this.bullets = [];
        this.visualEffects = [];
        this.bombs = [];

        this.playerItems = [];

        this.paused = false;
        this.startPressed = false;

        this.getReady = true;
        this.startOfRound = false;
        this.endOfRound = false;

        this.blocks = MapService.createMapOne();

        for (let playerIndex in players) {
            const player = players[playerIndex];
            player.length = START_LENGTH * 2;
            player.tail = [];
            player.onetimeItem = null;
            player.permanentItems = [];

            let angle = playerIndex * degreeStep;
            player.head.x = (CANVAS_WIDTH / 2) + (distanceToCenter * Math.cos(degToRad(angle)));
            player.head.y = (CANVAS_HEIGHT / 2) + (distanceToCenter * Math.sin(degToRad(angle)));

            player.direction = angle - 180;

            player.onetimeItem = null;
            player.permanentItems = [];

            player.alive = true;
        }

        relocate(this.fruit, false, this.generateCollidableObjectArray());
    }

    /**
     * 
     */
    startGame() {
        this.startOfRound = false;
    }

    /**
     * @returns Boolean True if further logic processing should be skipped, due to game Interruption
     */
    handleKeyPresses() {
        // ---- key presses
        if (this.endOfRound === false && this.startOfRound === false) {
            Controller.singlePress("pause game", "start", (playerIndex, player, controller) => { 
                this.paused = !this.paused; 
                this.layers.getready.pauseGame();
            });
        }

        if (this.getReady === true) {
            this.layers.getready.logic();
        }

        if (this.paused === true) {
            Controller.singlePress("back to main screen", "north", (playerIndex, player, controller) => {
                switchScene(MainScene);
            });

            Controller.singlePress("fullscreen", "select", (playerIndex, player, controller) => {
                toggleFullscreen();
            });
        }

        if (this.getReady === true && this.paused === false) {
            Controller.allPlayerPressed("confirm readyness", "south", () => {
                this.layers.startofround.players = players;
                this.getReady = false;
                this.startOfRound = true;
                this.layers.startofround.resetCounter();
            });
        }

        if (this.getReady === false) {
            this.layers.getready.resetButtons();
        }
        
        if (this.endOfGame) {
            this.layers.endofgame.logic();
        }

        if (this.startOfRound) {
            this.layers.startofround.logic();
        }

        if (this.endOfRound) {
            this.layers.endofround.logic();
        }

        if (this.paused || this.getReady || this.endOfRound || this.startOfRound || this.endOfGame) return true;

        Controller.singlePress("shooting", "r2", (playerIndex, player, controller) => { this.callAction("shoot", [player.head.x, player.head.y, player.direction, player.playerIndex]); });

        Controller.singlePress("lay bomb", "l2", (playerIndex, player, controller) => { this.callAction("useOneTimeItem", [player]); });
        // ---- end of key presses
    }

    /**
     * 
     */
    handlePlayerControls() {
        for (let playerIndex in players) {
            let player = players[playerIndex];
            let controller = controls[playerIndex];

            // steering
            let steering = this.currentSteerSpeed;

            steering *= Math.abs(controller.dx);

            // controller input (only if the special effect is not in place)
            if (
                this.special === null ||
                this.special.isCollected === false ||
                this.special.announcmentIsActive === true ||
                this.special.type !== Special.types.SQUARED
            ) {
                if (controller.left) player.direction -= steering;
                if (controller.right) player.direction += steering;

                if (this.special !== null && this.special.type === Special.types.DISTORTED && this.special.announcmentIsActive === false && this.special.isCollected === true) {
                    player.direction += (DISTORTION_STEERING * Math.random()) - (DISTORTION_STEERING / 2)
                }
            }

            // accelaration/decelaration
            if (controller.boost || controller.south) player.speed = BOOST;
            if (controller.break || controller.west) player.speed = BREAK;
            if (!controller.boost && !controller.west && !controller.south && !controller.break) player.speed = SPEED;
        }
    }

    /**
     * @returns nothing
     */
    handleSpecialTime() {
        // tell the special item that the announcment is over
        if (
            this.special !== null &&
            this.special.announcmentIsActive === true &&
            this.special.t >= this.special.announcmentDuration
        ) {
            this.special.announcmentOver(this);
        }

        // special time is over
        if (
            this.special !== null &&
            this.special.announcmentIsActive == false &&
            this.special.t > this.special.duration
        ) {
            this.special.timedOut();
            this.special = null;
        }
    }

    /**
     * 
     */
    handleGarbageCollection() {
        this.bullets = this.bullets.filter(b => b.t < MAX_BULLET_TIME);
        this.bombs = this.bombs.filter(b => !b.exploded);
        this.playerItems = this.playerItems.filter(pi => pi.isDestroyed === false);
        this.visualEffects = this.visualEffects.filter( ve => !ve.completed() );
        this.blocks = this.blocks.filter( b => b.health > 0);
    }

    /**
     * @returns nothing
     */
    handleLogicUpdates() {
        const updateables = [
            this.special,
            this.pacman,
            ...this.bullets,
            ...this.bombs,
            ...this.playerItems,
            ...this.visualEffects,
            ...players
        ];

        for (let updateable of updateables) {
            if (updateable === null) continue;
            updateable.logic(this.frameCtr, this);
        }
    }

    /**
     * this operation is heavy but only necessary once a frame.
     * so lets cache it.
     * @returns Array<Object2D>
     */
    collidableObjectArrayCacheGenerationFrame = null;
    collidableObjectArrayCache = null;
    generateCollidableObjectArray() {
        if (
            this.collidableObjectArrayCache &&
            this.frameCtr === this.collidableObjectArrayCacheGenerationFrame
        ) return this.collidableObjectArrayCache;

        let objectsToCheck = [
            ...this.visualEffects,
            ...this.playerItems,
            ...this.bullets,
            ...this.blocks,
            ...this.bombs,
            this.pacman,
            this.fruit,
            this.special,
        ];

        for (let p of players) {
            if (p.active === false) continue;

            objectsToCheck.push(p.head);

            for (let tailItemIndex in p.tail) {
                // schmutziger hack um nicht zu viele elemente bei der collission zu betrachten
                if (tailItemIndex%this.collider.everyOtherTailItemValue !== 0) continue;
                
                objectsToCheck.push(p.tail[tailItemIndex]);
            }
        }

        objectsToCheck = objectsToCheck.filter(o => o !== null);

        this.collidableObjectArrayCache = objectsToCheck;
        this.collidableObjectArrayCacheGenerationFrame = this.frameCtr;

        return this.collidableObjectArrayCache;
    }

    /**
     * @returns Array<Player> Returns an array of players that had a collission.
     */
    handleCollissions() {
        const collidedPlayers = this.collider.checkCollisions(this.generateCollidableObjectArray(), this);
        
        return collidedPlayers;
    }

    /**
     * @returns nothing
     */
    handleEndOfRound(collidedPlayers) {
        // MARK LOOSERS
        for (let player of collidedPlayers) {
            player.alive = false;

            SoundService.playSound("fail");
        }

        // add loosers length to their buckets
        let numActivePlayers = players.filter(p => p.alive).length;
        let numPlayers = players.length;

        for (let player of collidedPlayers) {
            player.lastMultiplikator = (numPlayers - numActivePlayers);
            player.bucket += (player.length - START_LENGTH) * (numPlayers - numActivePlayers);
            player.lastLength = (player.length - START_LENGTH);
        }

        // end of round (no alive player exists enymore)
        const minAlivePlayer = players.length === 1 ? 0 : 1;
        if (players.filter(player => player.alive).length <= minAlivePlayer) {
            const winner = players.filter(player => player.alive).pop();

            if (winner) {
                winner.wins += 1;
                winner.bucket += (winner.length - START_LENGTH) * numPlayers;
                winner.lastMultiplikator = numPlayers;
                winner.lastLength = (winner.length - START_LENGTH);
                this.layers.endofround.winner = winner;
                if(winner.wins === WINNING_SCORE){
                    this.endOfGame = true;
                    this.layers.endofgame.winner = winner;
                }
            }

            if (!this.endOfGame) {
                this.endOfRound = true;
                this.layers.endofround.resetCounter();
            }
        }
    }

    /**
     * Main logic loop of game
     * 
     * @returns nothing
     */
    logic() {
        const gameIsInterrupted = this.handleKeyPresses();

        if (gameIsInterrupted) {
            return;
        }

        this.frameCtr++;

        this.handleSpecialTime();

        this.handlePlayerControls();

        this.handleGarbageCollection();

        this.handleLogicUpdates();

        const collidedPlayers = this.handleCollissions();

        this.handleEndOfRound(collidedPlayers);
    }

    /**
     * Main draw loop of game
     * 
     * @returns nothing
     */
    draw() {
        this.drawCtr++;

        if (this.special !== null && this.special.announcmentIsActive === false && this.special.isCollected === true) {
            context.save();

            if (this.special.type === Special.types.DISTORTED) {
                context.translate((Math.random() * DISTORTION_OFFSET) - (DISTORTION_OFFSET / 2), 0);
            }

            // -- ROLL BARREL AROUND
            if (this.special.type === Special.types.BARREL) {
                if (this.special.payload === null) {
                    this.special.payload = {
                        rotation: 0,
                        lastTatDraw: null,
                    };
                }

                const completion = (this.special.t / this.special.duration);
                const sinpi = Math.sin(Math.PI * completion);
                const sinpi75 = Math.sin(Math.PI * .98 * completion);

                context.translate(CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2);
                let scaleFactor = CANVAS_HEIGHT / Math.sqrt(2 * Math.pow(CANVAS_HEIGHT, 2));

                scaleFactor = 1 - ((1 - scaleFactor) * sinpi);

                context.scale(scaleFactor, scaleFactor);

                if (completion > .90) {
                    let framesLeft = this.special.duration - this.special.t;
                    let rotationLeft = 360 - (this.special.payload.rotation % 360);

                    let rotateThisFrame = rotationLeft / framesLeft;

                    // --- make up for draw time vs logic time
                    if (this.special.payload.lastTatDraw !== null) {
                        rotateThisFrame *= (this.special.t - this.special.payload.lastTatDraw);
                    }
                    this.special.payload.lastTatDraw = this.special.t;
                    // ---

                    this.special.payload.rotation = (this.special.payload.rotation + rotateThisFrame) % 360;
                } else {
                    this.special.payload.rotation = (this.special.payload.rotation + (SPECIAL_ROTATION_ANGLE * sinpi75) % 360);
                }

                context.rotate(degToRad(this.special.payload.rotation));
                context.translate(-CANVAS_HEIGHT / 2, -CANVAS_HEIGHT / 2);
            }
            // -- END OF ROLL BARREL AROUND
        }

        this.layers.background.draw(context, this.frameCtr, this);
        this.layers.frame.draw(context, this.frameCtr, this);

        // draw everything only when everybody is ready
        if (!this.getReady) {
            if (this.special !== null) {
                this.special.draw(context, this.frameCtr, this);
            }

            for (let bullet of this.bullets) {
                bullet.draw(context, this.frameCtr, this);
            }

            for (let bomb of this.bombs) {
                bomb.draw(context, this.frameCtr, this);
            }

            // update triggeredPlayerItems
            for (let item of this.playerItems) {
                item.draw(context, this.frameCtr, this);
            }

            this.fruit.draw(context, this.frameCtr, this);

            if (this.pacman !== null) {
                this.pacman.draw(context, this.frameCtr, this);
            }
            
            for (let block of this.blocks) {
                block.draw(context, this.frameCtr, this);
            }

            for (let visualEffect of this.visualEffects) {
                visualEffect.draw(context, this.frameCtr, this);
            }

            for (let player of players) {
                player.draw(context, this.frameCtr, this);
            }
            
        }
        
        if (this.special !== null && this.special.announcmentIsActive === false && this.special.isCollected === true) {
            context.restore();
        }

        if (this.getReady) {
            this.layers.getready.draw(context, this.frameCtr, this);
        }
        
        if (this.paused) {
            this.layers.paused.draw(context, this.frameCtr, this);
        }
        
        if (this.endOfGame) {
            this.layers.endofgame.draw(context, this.frameCtr, this);
        }
        
        if (this.startOfRound) {
            this.layers.startofround.draw(context, this.frameCtr, this);
        }

        if (this.endOfRound) {
            this.layers.endofround.draw(context, this.frameCtr, this);
        }
    }
}