class MainScene {
    numPlayer = 2;
    selection = 0;
    startPressed = false;

    layers = {};

    audioFiles = {
    };

    init() {
        this.layers.intro = new IntroLayer();

        Controller.resetLinks();
        players = [];
        controls = [];
    }

    reset() {

    }

    logic() {
        Controller.singlePress("start game", "start", (playerIndex, player, controller) => {
            switchScene(SelectColorScreen);
        });

        Controller.singlePress("fullscreen", "select", (playerIndex, player, controller) => {
            toggleFullscreen();
        });
    }

    draw() {
        this.layers.intro.draw(context, this.frameCtr, this);
        // this.layers.frame.draw(context, this.frameCtr, this);

        context.textAlign = 'center'; 
        context.font = "60px GAME_FONT";
        context.fillStyle = 'rgba(255, 255, 255, 1)';

        let text = "Press Start / Enter"
        /*let text = "Press any Key";
        if (players.length > 0) {
            text = `Press Start`
        }*/

        context.fillText(text, (CANVAS_WIDTH/2), CANVAS_HEIGHT/1.2,);

        /*
            if (players.length > 0) {
            text = `Number of Players: ${players.length} `
            context.fillText(text, (CANVAS_WIDTH/2), CANVAS_HEIGHT/1.3,);
            }
        */
    }
}