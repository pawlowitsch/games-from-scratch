/**
 * This is the place where the collision functions are registered.
 * Each collision function specifies what happens when A collides with B.
 * A and B are both of certain classes that extends the object2D class.
 * 
 * In addition to that, this function tells the Collider, what classes are "passive only".
 * Objects can collide with "passive only" objects. But "passive only" objects are not used
 * for the outer iteration in the collision detection. (due to better performance)
 * 
 * @param {*} collider 
 */
function registerColliderFunctionsToCollider(collider) {

    /**
     * 
     */
    collider.onlyPassiveClasses.push(Block);
    collider.onlyPassiveClasses.push(PlayerTail);
    collider.onlyPassiveClasses.push(Fruit);
    collider.onlyPassiveClasses.push(Bomb);
    collider.onlyPassiveClasses.push(Special);

    /**
     * 
     */
    collider.addColliderFunction(Bullet, PlayerPart, (bullet, playerPart, gameScene, collidedPlayers) => {
        const player = playerPart.relatedPlayer;

        if (player.alive === false) return;
        if (bullet.playerIndex === player.playerIndex) return;

        if (player.length >= START_LENGTH + INCREASE_LENGTH_VALUE) {
            player.length -= INCREASE_LENGTH_VALUE;
        } else {
            collidedPlayers.push(player);
        }

        SoundService.playSound("whoomp");
        gameScene.addVisualEffect(new ExplodeEffect(bullet.x, bullet.y));

        bullet.t = Number.MAX_SAFE_INTEGER;
    });

    /**
     * 
     */
    collider.addColliderFunction(Bullet, Block, (bullet, block, gameScene, collidedPlayers) => {
        block.health--;
        bullet.t = Number.MAX_SAFE_INTEGER;

        SoundService.playSound("whoomp");
        gameScene.addVisualEffect(new ExplodeEffect(bullet.x, bullet.y));
    });

    /**
     * 
     */
    collider.addColliderFunction(Bullet, Bomb, (bullet, bomb, gameScene, collidedPlayers) => {
        bullet.t = Number.MAX_SAFE_INTEGER;
        bomb.explode(bullet.playerIds, players[bullet.playerIds], gameScene);

        SoundService.playSound("whoomp");
        gameScene.addVisualEffect(new ExplodeEffect(bullet.x, bullet.y));
    });

    /**
     * 
     */
    collider.addColliderFunction(Bullet, Pacman, (bullet, pacman, gameScene, collidedPlayers) => {
        bullet.t = Number.MAX_SAFE_INTEGER;
        pacman.health--;

        if (this.pacman.health === 0) {
            pacman.die(gameScene, true);
            gameScene.pacman = null;
        }

        SoundService.playSound("whoomp");
        gameScene.addVisualEffect(new ExplodeEffect(bullet.x, bullet.y));
    });

    /**
     * 
     */
    collider.addColliderFunction(Bullet, PlayerItem, (bullet, playerItem, gameScene, collidedPlayers) => {
        bullet.t = Number.MAX_SAFE_INTEGER;
        playerItem.destroy();

        SoundService.playSound("whoomp");
        gameScene.addVisualEffect(new ExplodeEffect(bullet.x, bullet.y));
    });

    /**
     * 
     */
    collider.addColliderFunction(PlayerItem, Block, (playerItem, block, gameScene, collidedPlayers) => {
        if (playerItem.isCollected === false || playerItem.isActive === false) return;

        playerItem.destroy();

        block.health--;

        if (playerItem instanceof AreaEffectPlayerItem) {
            gameScene.addVisualEffect(new ExplodeEffect(block.x, block.y));
        } else {
            gameScene.addVisualEffect(new ExplodeEffect(playerItem.x, playerItem.y));
        }
        
        SoundService.playSound("whoomp");
    });

    /**
     * 
     */
    collider.addColliderFunction(Pacman, Fruit, (pacman, fruit, gameScene, collidedPlayers) => {
        let b = new Bomb();
        b.x = fruit.x;
        b.y = fruit.y;
        relocate(fruit, false, gameScene.generateCollidableObjectArray());
        bombs.push(b);
    });

    /**
     * 
     */
    collider.addColliderFunction(Pacman, PlayerHead, (pacman, playerHead, gameScene, collidedPlayers) => {
        const player = playerHead.relatedPlayer;
        if (player.alive === false) return;

        collidedPlayers.push(player);
    });

    /**
     * 
     */
    collider.addColliderFunction(Pacman, PlayerTail, (pacman, tailPart, gameScene, collidedPlayers) => {
        const player = tailPart.relatedPlayer;
        if (player.alive === false) return;

        tailPart.isHole = true;
    });

    /**
     * 
     */
    collider.addColliderFunction(PlayerHead, PlayerPart, (playerHead, playerPart, gameScene, collidedPlayers) => {
        const p0 = playerHead.relatedPlayer;
        const p1 = playerPart.relatedPlayer;

        if (p0.active === false || p1.active === false) return;

        if (
            p0.playerIndex === p1.playerIndex &&
            playerPart instanceof PlayerTail &&
            playerPart.tooCloseToHead
        ) {
            return;
        }

        collidedPlayers.push(p0);
    });

    /**
     * (1/2) Player collects player item on playfield
     */
    collider.addColliderFunction(PlayerHead, PlayerItem, (playerHead, playerItem, gameScene, collidedPlayers) => {
        const player = playerHead.relatedPlayer;
        if (player.alive === false) return;
        if (playerItem.isCollected === true) return;

        if (playerItem instanceof ActivePlayerItem && player.onetimeItem) {
            playerItem.destroy();
            return;
        }

        SoundService.playSound("coin");
        playerItem.collectedBy(player);
    });

    /**
     * (2/2) Player gets hit by playeritem (rocket)
     */
    collider.addColliderFunction(ActivePlayerItem, PlayerPart, (playerItem, playerPart, gameScene, collidedPlayers) => {
        const player = playerPart.relatedPlayer;

        if (player.alive === false) return;
        if (playerItem.canCollide === false || playerItem.isDestroyed) return;
        if (playerItem instanceof AreaEffectPlayerItem && playerItem.playerThatCollectedItem.playerIndex === playerPart.relatedPlayer.playerIndex) return;

        if (player.length >= START_LENGTH + INCREASE_LENGTH_VALUE) {
            player.length -= INCREASE_LENGTH_VALUE;
        } else {
            collidedPlayers.push(player);
        }

        playerItem.destroy();

        SoundService.playSound("boom1");
        gameScene.addVisualEffect(new ExplodeEffect(playerPart.x, playerPart.y));
    });

    /**
     * 
     */
    collider.addColliderFunction(ActivePlayerItem, ActivePlayerItem, (playerItem0, playerItem1, gameScene, collidedPlayers) => {
        if (playerItem0.canCollide === false || playerItem0.isDestroyed) return;
        if (playerItem1.canCollide === false || playerItem1.isDestroyed) return;

        playerItem0.destroy();
        playerItem1.destroy();

        if (!(playerItem0 instanceof AreaEffectPlayerItem)) {
            gameScene.addVisualEffect(new ExplodeEffect(playerItem0.x, playerItem0.y));
        }

        if (!(playerItem1 instanceof AreaEffectPlayerItem)) {
            gameScene.addVisualEffect(new ExplodeEffect(playerItem1.x, playerItem1.y));
        }
    });

    /**
     *
     */
    collider.addColliderFunction(PlayerHead, Bomb, (playerHead, bomb, gameScene, collidedPlayers) => {
        const player = playerHead.relatedPlayer;
        if (player.alive === false) return;

        collidedPlayers.push(player);
        bomb.explode(player.playerIndex, player, gameScene);

        SoundService.playSound("whoomp");
        gameScene.addVisualEffect(new ExplodeEffect(playerHead.x, playerHead.y));
    });

    /**
     * 
     */
    collider.addColliderFunction(PlayerHead, Fruit, (playerHead, fruit, gameScene, collidedPlayers) => {
        const player = playerHead.relatedPlayer;
        if (player.alive === false) return;
        
        gameScene.callAction("increaseLength", [player]);

        gameScene.gameSpeedIncrease += SPEED_INCREASE;
        gameScene.currentSteerSpeed += SPEED_INCREASE;

        SoundService.playSound("pop");
        gameScene.addVisualEffect(
            new CollectEffect(
                player.head.x,
                player.head.y,
                player.playerIndex
            )
        );

        relocate(fruit, false, gameScene.generateCollidableObjectArray());
        gameScene.callAction("placePlayerItem");
    });

    /**
     * Player collects special item
     */
    collider.addColliderFunction(PlayerHead, Special, (playerHead, special, gameScene, collidedPlayers) => {
        const player = playerHead.relatedPlayer;
        if (player.alive === false) return;
        if (special.isCollected === true) return;

        gameScene.addVisualEffect(
            new CollectEffect(
                (player.head.x + special.x) / 2,
                (player.head.y + special.y) / 2,
                player.playerIndex
            )
        );

        SoundService.playSound("announcement");
        gameScene.special.collected(player.playerIndex, player, gameScene);
    });

    /**
     * Player hits a block
     */
    collider.addColliderFunction(PlayerHead, Block, (playerHead, block, gameScene, collidedPlayers) => {
        const player = playerHead.relatedPlayer;
        if (player.alive === false) return;

        block.health--;

        collidedPlayers.push(playerHead.relatedPlayer);

        gameScene.addVisualEffect(new ExplodeEffect(playerHead.x, playerHead.y));
    });
}