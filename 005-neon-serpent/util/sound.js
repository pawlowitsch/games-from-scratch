class SoundService {

    static audioFiles = {
        fail: new Audio('sounds/fail.mp3'),
        coin: new Audio('sounds/coin.mp3'),
        pop: new Audio('sounds/pop.mp3'),
        shoot: new Audio('sounds/shoot.mp3'),
        emptyShot: new Audio('sounds/shoot_empty.wav'),
        announcement: new Audio('sounds/announcement.wav'),
        ching: new Audio('sounds/ching.mp3'),
        boom: new Audio('sounds/boom.mp3'),
        tailTime: new Audio('sounds/ItsTailTime_loud.mp3'),
        music: new Audio('sounds/ItIs.mp3'),
        ding: new Audio('sounds/ding.mp3'),
        whoomp: new Audio('sounds/whoomp.mp3'),
        boom1: new Audio('sounds/boom1.mp3'),
    };

    static playSound(sound) {
        let audio;

        if (this.audioFiles[sound]) {
            audio = this.audioFiles[sound];
            audio.currentTime = 0;
            audio.loop = false;
            audio.play();
        }
    }
}