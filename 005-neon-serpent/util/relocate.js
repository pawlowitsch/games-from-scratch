function relocate(object, center, collidableObjects) {
    if (notset(collidableObjects)) throw new Error("collidableObjects not set for relocate call");

    if (center) {
        object.x = CANVAS_WIDTH / 2;
        object.y = CANVAS_HEIGHT / 2;
        return;
    }

    let nearPlayer;
    let relocateTryCounter = 0;
    do {
        relocateTryCounter++;
        nearPlayer = false;

        object.x = FRUIT_BORDER + Math.floor((CANVAS_WIDTH - (FRUIT_BORDER * 2)) * Math.random());
        object.y = FRUIT_BORDER + Math.floor((CANVAS_HEIGHT - (FRUIT_BORDER * 2)) * Math.random());

        // TODO: Collision distance should not be the same for everything
        const dist = 2 * FRUIT_COLLISION_RADIUS;
        for (let collidableObject of collidableObjects) {
            if (collidableObject === object) continue;

            if (distanceSquared(collidableObject, object) <= dist*dist ) {
                nearPlayer = true;
                break;
            }
        }
    } while (nearPlayer);
}