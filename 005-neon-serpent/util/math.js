const degToRad = (deg) => {
    return Math.PI * deg / 180;
};

const radToDeg = (rad) => {
    return rad * ( 180 / Math.PI);
};

function getAngleDegrees(fromX,fromY,toX,toY) {
    let deltaX = fromX - toX;
    let deltaY = fromY - toY;
    let radians = Math.atan2(deltaY, deltaX)
    let degrees = ((radians * 180) / Math.PI) - 90;
    
    return degrees;
}

const distance = (p0, p1) => {
    return Math.sqrt((Math.pow(p0.x-p1.x,2))+(Math.pow(p0.y-p1.y,2)));
};

const distanceSquared = (p0, p1) => {
    return (Math.pow(p0.x-p1.x, 2))+(Math.pow(p0.y-p1.y, 2));
};

function circleIntersectsRectangle(circleX, circleY, circleR, rectX, rectY, rectW, rectH) {
    let distance = {
        x: Math.abs(circleX - rectX),
        y: Math.abs(circleY - rectY)
    }

    if (distance.x > (rectW/2 + circleR)) return false;
    if (distance.y > (rectH/2 + circleR)) return false;

    if (distance.x <= (rectW/2)) return true;
    if (distance.y <= (rectH/2)) return true;

    const cornerDistSquared = Math.pow(distance.x - rectW/2, 2) + Math.pow(distance.y - rectH/2, 2);

    return cornerDistSquared <= Math.pow(circleR, 2);
}