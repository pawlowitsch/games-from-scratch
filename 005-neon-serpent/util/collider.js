class Collider {
    onlyPassiveClasses = [];

    /**
     * hacky value for only considering every n'th tail item
     * so that we do not have too many objects to consider for collission.
     * Value of 1 means, that every tailItem ist considered for collission (no tweak).
     * A Value > 1 means, that every n'th item (n = everyOtherTailItemValue) is considered for collision.
     */
    everyOtherTailItemValue = 1;

    registeredColliderFunctions = [
        /*
        {
            classA: Class,
            classB: Class,
            fn: (obj0, obj1, scene) => {
                
            }
        }
        */
    ];

    addColliderFunction(classA, classB, fn) {
        this.registeredColliderFunctions.push({
            classA, classB, fn
        })
    }

    checkCollisions(allObjects, gameScene) {
        let maxCollisionDistance = 0;
        allObjects = allObjects.filter(o => {
            if (o instanceof Circle) maxCollisionDistance = Math.max(maxCollisionDistance, o.r);
            if (o instanceof Rect) maxCollisionDistance = Math.max(maxCollisionDistance, o.w, o.h);

            return o !== null;
        });

        const qtreeSelectionSize = 2 * maxCollisionDistance;

        const qtree = new QuadTree(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        
        for (let obj2d of allObjects) {
            qtree.insert(obj2d);
        }

        const collissionsFound = [];
        const collidedPlayersFound = [];

        // Check collission of player Heads w/ walls
        for (let player of players) {
            if (player.alive === false) continue;
            if (player.head.x <= 0 || player.head.y <= 0 || player.head.x >= CANVAS_WIDTH || player.head.y >= CANVAS_HEIGHT) {
                collidedPlayersFound.push(player);
            }
        }

        const onlyActiveObjects = allObjects.filter(o => {
            for (let onlyPassiveClass of this.onlyPassiveClasses) {
                if (o instanceof onlyPassiveClass) return false;
            }
            return true;
        });

        for (let obj2d of onlyActiveObjects) {
            let nearbyObjects = qtree.inRect(
                obj2d.x-(qtreeSelectionSize/2),
                obj2d.y-(qtreeSelectionSize/2),
                qtreeSelectionSize,
                qtreeSelectionSize
            );

            for (let otherObj2d of nearbyObjects) {
                // check if both items are the same
                if (obj2d === otherObj2d) continue;

                // check if the objects already collided
                if (collissionsFound.indexOf(`${obj2d.id}:${otherObj2d.id}`) !== -1) continue;

                // check if a player behind playerpart is already inactive
                if (obj2d instanceof PlayerPart && obj2d.relatedPlayer.active === false) continue;
                if (otherObj2d instanceof PlayerPart && otherObj2d.relatedPlayer.active === false) continue;

                // check if a player behind a playerpart already collided
                if (obj2d instanceof PlayerPart && collidedPlayersFound.indexOf(obj2d.relatedPlayer) !== -1) continue;
                if (otherObj2d instanceof PlayerPart && collidedPlayersFound.indexOf(otherObj2d.relatedPlayer) !== -1) continue;

                let collissionFound = false;

                if (obj2d instanceof Circle && otherObj2d instanceof Circle) {
                    const dist = obj2d.r + otherObj2d.r;
                    if (distanceSquared(obj2d, otherObj2d) <= dist*dist) {
                        collissionFound = true;
                    }
                }

                if (obj2d instanceof Rect && otherObj2d instanceof Rect) {
                    // TODO: MISSING COLLISSION RECT/RECT
                }

                if (obj2d instanceof Circle && otherObj2d instanceof Rect) {
                    if (circleIntersectsRectangle(obj2d.x, obj2d.y, obj2d.r, otherObj2d.x, otherObj2d.y, otherObj2d.w, otherObj2d.h)) {
                        collissionFound = true;
                    }
                }

                if (obj2d instanceof Rect && otherObj2d instanceof Circle) {
                    if (circleIntersectsRectangle(otherObj2d.x, otherObj2d.y, otherObj2d.r, obj2d.x, obj2d.y, obj2d.w, obj2d.h)) {
                        collissionFound = true;
                    }
                }

                if (collissionFound === true) {
                    collissionsFound.push(`${obj2d.id}:${otherObj2d.id}`);
                    collissionsFound.push(`${otherObj2d.id}:${obj2d.id}`);

                    // (1/2) find registered collider functions for collission (A, B)
                    this.registeredColliderFunctions.filter(registeredItem => {
                        return obj2d instanceof registeredItem.classA && otherObj2d instanceof registeredItem.classB;
                    }).forEach( registeredItem => {
                        registeredItem.fn(obj2d, otherObj2d, gameScene, collidedPlayersFound);
                    });

                    // (2/2) find registered collider functions for collission (B, A)
                    this.registeredColliderFunctions.filter(registeredItem => {
                        return obj2d instanceof registeredItem.classB && otherObj2d instanceof registeredItem.classA;
                    }).forEach( registeredItem => {
                        registeredItem.fn(otherObj2d, obj2d, gameScene, collidedPlayersFound);
                    });
                }

            }
        }

        return collidedPlayersFound;
    }
}