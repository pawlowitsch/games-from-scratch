class QuadTree {
    nw = null;
    ne = null;
    sw = null;
    se = null;

    capacity = 10;

    items = [];

    x = null;
    y = null;
    w = null;
    h = null;

    constructor(x, y, w, h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    insert(item) {
        if (this.items.length >= this.capacity) {
            let center = {
                x: this.x + this.w/2,
                y: this.y + this.h/2,
            };

            let quartal;

            // nw
            if (item.y < center.y && item.x < center.x) {
                if (this.nw === null) {
                    this.nw = new QuadTree(this.x, this.y, this.w/2, this.h/2);
                }
                quartal = this.nw;
            }

            // ne
            if (item.y < center.y && item.x >= center.x) {
                if (this.ne === null) {
                    this.ne = new QuadTree(this.x+this.w/2, this.y, this.w/2, this.h/2);
                }
                quartal = this.ne;
            }

            // sw
            if (item.y >= center.y && item.x < center.x) {
                if (this.sw === null) {
                    this.sw = new QuadTree(this.x, this.y+this.h/2, this.w/2, this.h/2);
                }
                quartal = this.sw;
            }

            // se
            if (item.y >= center.y && item.x >= center.x) {
                if (this.se === null) {
                    this.se = new QuadTree(this.x+this.w/2, this.y+this.h/2, this.w/2, this.h/2);
                }
                quartal = this.se;
            }

            quartal.insert(item);
            return;
        }

        this.items.push(item);
    }

    inRect(x, y, w, h) {
        if (x+w < this.x) return [];
        if (x > this.x+this.w) return [];
        if (y+h < this.y) return [];
        if (y > this.y+this.h) return [];

        let foundItems = [];

        for (let item of this.items) {
            if (
                item.x >= x && item.x <= x+w &&
                item.y >= y && item.y <= y+h
            ) {
                foundItems.push(item);
            }
        }

        if (this.nw) foundItems = [ ...foundItems, ...this.nw.inRect(x, y, w, h) ];
        if (this.ne) foundItems = [ ...foundItems, ...this.ne.inRect(x, y, w, h) ];
        if (this.sw) foundItems = [ ...foundItems, ...this.sw.inRect(x, y, w, h) ];
        if (this.se) foundItems = [ ...foundItems, ...this.se.inRect(x, y, w, h) ];

        return foundItems;
    }
}