function isMobile() {
    return navigator.userAgent.indexOf("iPhone") !== -1 ||
        navigator.userAgent.indexOf("iPad") !== -1 ||
        navigator.userAgent.indexOf("Android") !== -1;
}