class MapService {
    static createMapOne() {
        const blocks = [];

        const top = 200;
        const bottom = CANVAS_HEIGHT - top;
        const left = 400;
        const right = CANVAS_WIDTH - left;

        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(left + (i*BLOCK_SIZE), top)
            );
        }
        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(left, bottom - BLOCK_SIZE*8 + (i*BLOCK_SIZE))
            );
        }
        
        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(left + (i*BLOCK_SIZE), bottom)
            );
        }
        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(right, bottom - (BLOCK_SIZE*8) + (i*BLOCK_SIZE))
            );
        }
        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(left, top + BLOCK_SIZE + (i*BLOCK_SIZE))
            );
        }
        

        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(right - (i*BLOCK_SIZE), bottom)
            );
        }

        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(right - (i*BLOCK_SIZE), top)
            );
        }
        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(right, top + BLOCK_SIZE + (i*BLOCK_SIZE))
            );
        }
        for (let i=0;i<13;i++) {
            blocks.push(
                new Block(CANVAS_WIDTH - (BLOCK_SIZE*8) - (i*BLOCK_SIZE), CANVAS_HEIGHT/2)
            );
        }
        for (let i=0;i<13;i++) {
            blocks.push(
                new Block((BLOCK_SIZE*8) + (i*BLOCK_SIZE), CANVAS_HEIGHT/2)
            );
        }
        for (let i=0;i<21;i++) {
            blocks.push(
                new Block((BLOCK_SIZE*8), (CANVAS_HEIGHT/2)-(BLOCK_SIZE*10)+(i*BLOCK_SIZE))
            );
        }
        for (let i=0;i<21;i++) {
            blocks.push(
                new Block(CANVAS_WIDTH-(BLOCK_SIZE*8), (CANVAS_HEIGHT/2)-(BLOCK_SIZE*10)+(i*BLOCK_SIZE))
            );
        }
        for (let i=0;i<8;i++) {
            blocks.push(
                new Block(CANVAS_WIDTH/2, (CANVAS_HEIGHT/2)-(BLOCK_SIZE*3) + (i*BLOCK_SIZE))
            );
        }
        
        return blocks;
    }
}