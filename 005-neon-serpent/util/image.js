class ImageService {
    static images = {}

    static loadImages() {
        ImageService.images.special = new Image();
        ImageService.images.special.src = "./graphics/special.svg";

        ImageService.images.crosshair = new Image();
        ImageService.images.crosshair.src = "./graphics/crosshair.svg";

        ImageService.images.torpedo = new Image();
        ImageService.images.torpedo.src = "./graphics/torpedo.svg";
        
        ImageService.images.smoke1 = new Image();
        ImageService.images.smoke1.src = "./graphics/effects/smoke1.svg";
        
        ImageService.images.wallcrack = new Image();
        ImageService.images.wallcrack.src = "./graphics/effects/wallcrack.svg";
    }
}