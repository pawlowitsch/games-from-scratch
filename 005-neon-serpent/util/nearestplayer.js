function getNearestTargetPlayer(x, y, ignoredPlayers) {
    if (notset(ignoredPlayers)) ignoredPlayers = [];

    ignoredPlayers = ignoredPlayers.map(p => p.playerIndex);

    let targetPlayer = null;
    let targetPlayerDistance = Number.POSITIVE_INFINITY;

    // TODO: Bennis Einwand mit "erster spieler ist doch eh bla bla"
    for (let possibleTargetPlayer of players) {
        if (possibleTargetPlayer.alive === false) continue;
        if (ignoredPlayers.indexOf(possibleTargetPlayer.playerIndex)!==-1) continue;

        const distToPossibleTargetPlayer = distanceSquared({x, y}, possibleTargetPlayer.head);

        if ( distToPossibleTargetPlayer < targetPlayerDistance*targetPlayerDistance) {
            targetPlayer = possibleTargetPlayer;
            targetPlayerDistance = distToPossibleTargetPlayer;
        }
    }

    return targetPlayer;
}