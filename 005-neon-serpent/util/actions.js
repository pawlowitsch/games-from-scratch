function registerActions(gameScene) {
    gameScene.registerAction("placePlayerItem", () => {    
        if (Math.random() < PLAYERITEM_PLACEMENT_THRESHOLD) return;
    
        // --- select item clazz of possible item clazzes
        const clazzes = [ BlastItem, RocketItem, SlowdownItem ];
        if (clazzes.length === 0) return;
        const clazzIndex = Math.min(
            Math.floor(clazzes.length * Math.random()),
            clazzes.length - 1
        );
        const clazz = clazzes[clazzIndex];
        // --- end of item selection

        // place item on playfield
        const item = new clazz(0, 0);
        relocate(item, false, gameScene.generateCollidableObjectArray());
        gameScene.playerItems.push( item );
    });

    gameScene.registerAction("placeSpecial", () => {
        if (gameScene.special !== null) return;
    
        if (Math.random() < SPECIAL_PLACEMENT_THRESHOLD) return;
    
        gameScene.special = new Special(0, 0);
        relocate(gameScene.special, false, gameScene.generateCollidableObjectArray());
    });

    gameScene.registerAction("shoot", (x, y, direction, playerIndex) => {
        const player = players[playerIndex];
        if (player.alive === false){
            return;
        }
    
        if (player.length <= START_LENGTH) {
            SoundService.playSound("emptyShot");
            return;
        }
    
        player.length = Math.max(player.length - LENGTH_INCREASE, 0);
    
        SoundService.playSound("shoot");
    
        const bullet = new Bullet(x, y);
        bullet.direction = direction;
        bullet.playerIndex = playerIndex;
    
        gameScene.bullets.push(bullet);
    });

    gameScene.registerAction("useOneTimeItem", (player) => {    
        if(player.alive === false) return;
        if (player.onetimeItem === null) return;
    
        player.onetimeItem.x = player.head.x;
        player.onetimeItem.y = player.head.y;
        player.onetimeItem.direction = player.direction;
    
        gameScene.playerItems.push(player.onetimeItem);

        player.onetimeItem.use();
        player.onetimeItem = null;
    });

    gameScene.registerAction("layBomb", (playerIndex) => {
        const player = players[playerIndex];
        if(player.alive === false){
            return;
        }
    
        if (player.length <= START_LENGTH) {
            SoundService.playSound("emptyShot");
            return;
        }
    
        //TODO: figure out the real last drawn tail element
        let b = new Bomb();
        b.x = player.tail[player.tail.length-1].x;
        b.y = player.tail[player.tail.length-1].y;            
        this.bombs.push(b);
    
        player.length = Math.max(player.length - LENGTH_INCREASE, 0);
    });

    gameScene.registerAction("increaseLength", (player) => {
        player.length += INCREASE_LENGTH_VALUE;
    
        gameScene.maxLength = Math.max(player.length, gameScene.maxLength);
    });

    gameScene.registerAction("resetWins", () => {
        for (let playerIndex in players) {
            const player = players[playerIndex];
            player.wins = 0;
            gameScene.endOfGame = false;
        }
    });
}