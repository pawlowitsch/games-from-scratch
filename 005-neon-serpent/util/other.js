function notset(a) {
    return a === undefined || a === null;
}