class HUDLayer {
    draw(context, frameCtr, scene) {
        context.save();
        context.font = FONT_POINTS;

        // remember the index for colors
        let localPlayers = players.map((v, k) => {
            return { ...v, playerIndex: k, };
        });

        // sort the players by bucket
        localPlayers.sort((a, b) => {
            return b.bucket - a.bucket;
        });
        
        // draw the players
        for (let idx in localPlayers) {
            const player = localPlayers[idx];
            const playerIndex = player.playerIndex;
            const offsetY = 20 * idx;
            context.textAlign = 'left';
            context.fillStyle = player.color;
            context.shadowColor = COLOR_SHADOW;
            context.shadowOffsetY = 2;
            //context.fillText( `P${1+(1*playerIndex)}    W:${player.wins}    L:${player.length/LENGTH_INCREASE}    B:${(player.bucket/LENGTH_INCREASE).toFixed(1)}`, 10, 30 + offsetY );
            context.fillText( `P${1+(1*playerIndex)}: ${((player.length-START_LENGTH)/LENGTH_INCREASE)}`, 10, 30 + offsetY );
        }

        // draw the max length
        /*
            context.textAlign = 'left';
            context.fillStyle = "white";
            context.shadowColor = "black";
            context.fillText( `BEST: ${scene.maxLength/LENGTH_INCREASE}`, CANVAS_HEIGHT/2, 30);   
        */
        context.restore();
    }
}