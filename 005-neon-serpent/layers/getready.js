class GetReadyLayer {

    drawCtr = 0;

    confirmedPlayers = [];

    image = null;
    paused = false;
    
    constructor() {
        this.image = new Image();
        this.image.src = "./graphics/check.svg";
    }

    pauseGame() {
        this.paused = !this.paused;
    }

    logic() {
        if(this.paused === false){ 
            Controller.singlePress("playerConfirmed", "south", (playerIndex, player, controller) => {
                this.confirmedPlayers[playerIndex] = true;
                SoundService.playSound("ching");
            });
        }    
    }

    resetButtons() {
        this.confirmedPlayers = [];
    }

    // construct  image to show 
    getButtonImage(vendor, button) {
        this.imageButton = new Image();
        this.imageButton.src = `./graphics/gamepad/${vendor}_${button}.svg`;
    };

    wrightText(text, x, y) {
        context.fillStyle = 'rgba(255, 255, 255, 1)';
        context.font = "bold 15px GAME_FONT";
        context.textAlign = "center";
        context.font = "18px GAME_FONT";
        context.fillText(
            text,
            x,
            y,
        );
    }

    drawButton(vendor, button, x, y, targetImageSize){
        this.getButtonImage(vendor, button)
        context.beginPath();
        context.drawImage(
            this.imageButton,
            0, 0,
            this.imageButton.width, this.imageButton.height,
            x - (targetImageSize / 2), y - (targetImageSize),
            targetImageSize, targetImageSize
        );
        context.closePath();
    }

    draw(context, frameCtr, scene) {
        this.drawCtr++;

        // const maxScoreFunction = (acc, p) => Math.max(acc, p.bucket);
        // const scoreSortFunction = (a, b) => b.bucket - a.bucket;
        const bucketToPoints = (bucket) => {
            return bucket / LENGTH_INCREASE;
        };
        // const getScore = (clonedPlayer) => clonedPlayer.bucket;
        const getScore = (clonedPlayer) => clonedPlayer.wins;
        const maxScoreFunction = (acc, p) => Math.max(acc, p.wins);
        const scoreSortFunction = (a, b) => b.wins - a.wins;

        const clonedPlayers = players.map((player, playerIndex) => {
            let clone = Player.clonePlayer(player);
            clone.playerIndex = playerIndex;
            return clone;
        });
        clonedPlayers.sort(scoreSortFunction);

        let maxScore = clonedPlayers.reduce(maxScoreFunction, 0);

        const playerDrawXBorder = 500;

        const deltaY = 80;

        const backgroundHeight = (players.length * deltaY) + deltaY;
        const backgroundWidth = CANVAS_WIDTH - (playerDrawXBorder * 2) + 200;

        context.save();
        context.fillStyle = "rgba(0,0,0,.4)";
        context.beginPath();
        context.roundRect(playerDrawXBorder - 120, 30, backgroundWidth, backgroundHeight, [15]);
        context.fill();
        context.restore();


        for (let sortedplayerIndex in clonedPlayers) {
            const clonedPlayer = clonedPlayers[sortedplayerIndex];

            const drawTop = 100 + (sortedplayerIndex * deltaY);

            const yAmplitude = 20;
            const maxPointsInRail = 150;

            const maxX = CANVAS_WIDTH - playerDrawXBorder;
            const minX = playerDrawXBorder;

            const scoreRatio = getScore(clonedPlayer) / WINNING_SCORE;

            // let maxPlayerX = maxX * (getScore(clonedPlayer) / maxScore);
            let maxPlayerX = playerDrawXBorder + ((maxX-minX) * scoreRatio);

            // wenn bishzer keiner punkte gemacht hat, sollen alle tails 100% lang gemalt werden
            /*
            if (maxScore === 0) {
                maxPlayerX = maxX;
            }
            */

            // wenn der aktuelle spieler keine punkte gemacht hat, soll er nur 10 px breit gemalt werden
            /*
            if (maxScore !== 0 && getScore(clonedPlayer) === 0) {
                maxPlayerX = playerDrawXBorder + 10;
            }
            */

            // nur malen von head und tail. keine patronen oder counter
            clonedPlayer.drawOnlyPlayer = true;
            // nach rechts schauend
            clonedPlayer.direction = 0;
            clonedPlayer.head = { x: maxPlayerX, y: drawTop };
            // der tail ist ein array von {x: int, y: int} objecten (afaik)
            clonedPlayer.tail = [];
            
            // draw winning condition indicator (vertikale linien)
            for (let i = 1; i <= WINNING_SCORE; i++) {
                let dx = (maxX-minX) / WINNING_SCORE;
                context.save();
                context.strokeStyle = "white";
                context.beginPath();
                //let dx = (playerDrawXBorder+(i*((CANVAS_WIDTH-(playerDrawXBorder*2))/WINNING_SCORE)+((CANVAS_WIDTH-(playerDrawXBorder*2))/WINNING_SCORE)));        
                let dy =  35;      
                context.moveTo(minX+(dx*i), drawTop-dy);
                context.lineTo(minX+(dx*i), drawTop+deltaY-dy);
                context.stroke();
                context.restore();
            }
            
            // draw score tail
            let numberOfElementsInTail = Math.floor(maxPointsInRail * scoreRatio);
            let dx = (maxPlayerX - playerDrawXBorder) / numberOfElementsInTail;

            let maxDy = yAmplitude * (maxPlayerX/maxX);

            let maxXDistance = WINNING_SCORE * (Math.PI*2);

            if (clonedPlayer.wins > 0){
                for (let i = 0; i <= numberOfElementsInTail; i++) {
                    let dy = maxDy * Math.sin(
                        // sin verschiebung auf x achse
                        (
                            (Math.PI*2)*(this.drawCtr%100)/100
                        ) +

                        // abschnitt malen der kurve, jeweils ein (i/numberOfElementsInTail) von sin 0 bis 2PI
                        (
                            maxXDistance *
                            scoreRatio *
                            (i/numberOfElementsInTail)
                        )
                    );
    
                    clonedPlayer.tail.push(
                        // { x: maxPlayerX - (dx*i), y: drawTop + dy }
                        { x: playerDrawXBorder + (dx*i), y: drawTop + dy }
                        
                    );
                }
                // clonedPlayer.tail.push(clonedPlayer.head);
                // clonedPlayer.head = clonedPlayer.tail[clonedPlayer.tail.length-1];
                clonedPlayer.head = clonedPlayer.tail[0];
            }

            // draw point instead of tail

            else {
                context.save();
                context.beginPath();
                context.arc(playerDrawXBorder, drawTop, TAIL_WIDTH, 0, 2 * Math.PI, false);
                context.fillStyle = clonedPlayer.color;
                context.shadowOffsetY = SHADOW_OFFSET;
                context.shadowColor = COLOR_SHADOW;
                context.fill();
                context.restore();
            }


            // draw player name
            let addedPointsText = `gets ${clonedPlayer.lastMultiplikator} times ${bucketToPoints(clonedPlayer.lastLength)} tail points`;
            if (maxScore === 0) {
                addedPointsText = "";
            }

            const playerInfoText = `P${clonedPlayer.playerIndex + 1}`;
            context.textAlign = "left";
            context.font = "15px GAME_FONT";
            context.fillStyle = 'rgba(255, 255, 255, .9)';
            context.fillText(
                playerInfoText,
                playerDrawXBorder,
                drawTop - 20
            );

            // draw player bucket number
            if (maxScore > 0) {
                context.fillText(
                    `${"" + getScore(clonedPlayer)}`,
                    maxPlayerX + 20,
                    drawTop + 5
                );
            }

            // draw player confirmed
            if (this.confirmedPlayers[clonedPlayer.playerIndex] === true) {
                let targetImageSize = 40;
                context.beginPath();
                context.drawImage(
                    this.image,
                    0, 0,
                    this.image.width, this.image.height,
                    (playerDrawXBorder - 50) - (targetImageSize / 2), drawTop - (targetImageSize),
                    targetImageSize, targetImageSize
                );
                context.closePath();
            } else {
                // determ instruction keys by input type
                const inputIdentifier = Controller.getControllerIdentifierByplayerIndex(clonedPlayer.playerIndex);                
                let vendorId = Controller.getGampadVendorByplayerIndex(clonedPlayer.playerIndex);
                if (inputIdentifier.startsWith("keyboard-")) {
                    vendorId = "";
                }
                try {
                    if(vendorId.includes("XInput")){vendorId = "xinput"};
                    if(vendorId.includes("054c")){vendorId = "dualshock"};
                    if(vendorId.includes("057e")){vendorId = "nintendo"};
                } catch (e) {
                    vendorId = "xinputcon";
                }

                let readyKey = "south"
                if(inputIdentifier === "keyboard-0"){readyKey="^"};
                if(inputIdentifier === "keyboard-1"){readyKey = "W"};
                
                let targetImageSize = 38;
                let dx = (playerDrawXBorder - 38) - (targetImageSize / 2)
                let dy = drawTop + 40 - (targetImageSize);
                let size = 25;
                
                // draw instructions to ready up
                if (inputIdentifier.startsWith("gamepad-")) {
                    
                    this.drawButton(vendorId, readyKey, dx, dy, size);
                } else {
                    this.wrightText(readyKey, dx, dy);
                }   
            }

            clonedPlayer.draw(context, frameCtr, scene);
        }


        // --- draw get ready text incl. black banner
        const readButtonPosY = CANVAS_HEIGHT - 180;
        const readyButtonHeight = 100;
        const readyButtonWidth = readyButtonHeight * ASPECTRATIO;

        context.save();
        context.fillStyle = "rgba(255,255,255,1)";
        context.beginPath();
        context.roundRect((CANVAS_WIDTH/2)-(readyButtonWidth/2), readButtonPosY-(readyButtonHeight/2), readyButtonWidth, readyButtonHeight, [10]);
        context.fill();
        context.restore();


        context.textAlign = 'center';
        context.font = "bold 15px GAME_FONT";
        context.fillStyle = COLOR_BACKGROUND;
        context.fillText(
            `READY?`,
            CANVAS_WIDTH / 2,
            readButtonPosY + 5
        );
        // --- end of text

        context.restore();
    }
}