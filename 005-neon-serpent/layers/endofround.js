class endOfRoundLayer {
    onCountdownOverFn = null;

    winner = null;

    time = 0;

    fnInvoked = true;

    resetCounter() {
        this.fnInvoked = false;
        this.time = new Date().getTime();
    }

    logic() {
        let currentTime = new Date().getTime();

        if ((currentTime-this.time)>END_OF_ROUND_PAUSE) {

            if (this.fnInvoked === false) {

                if (this.onCountdownOverFn !== null) {
                    this.onCountdownOverFn();
                    this.winner = null;
                }
                this.fnInvoked = true;
            }
        }
    }

    draw(context) {
        const radius = GUI_RADIUS;

        let strokeStyle = GUI_COLOR;
        let fillText = 'DRAW';
        let arcX = CANVAS_WIDTH / 2;
        let arcY = CANVAS_HEIGHT / 2;

        if(this.winner){
            strokeStyle = this.winner.color;
            fillText = 'Player ' + (this.winner.playerIndex  + 1) + ' WINS!!!';
            arcX = this.winner.head.x;
            arcY = this.winner.head.y;
        }; 
  
        context.save(); 
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.fillStyle = "rgba(0,0,0,.1)";
        context.beginPath();
        
        context.arc(arcX, arcY, radius, 0, 2 * Math.PI);
        context.fill();
        context.lineWidth = 10;
        context.strokeStyle = strokeStyle;
        context.stroke();
        context.restore();

        context.save();
        context.textAlign = 'center';
        context.font = "bold 30px GAME_FONT";
        context.fillStyle = "white";
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.fillText(
            fillText,
            CANVAS_WIDTH / 2,
            (CANVAS_HEIGHT / 2) + 15,
        );
        context.restore();      
    }
}