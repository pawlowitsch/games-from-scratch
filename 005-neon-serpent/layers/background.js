class BackgroundLayer {
    backgroundFadeSwitch = false;

    drawCtr = 0;
    
    constructor() {
        this.isMobile = isMobile(); 
    }

    draw(context, frameCtr, scene) {
        if (this.isMobile) return;

        this.drawCtr++;

        const backgroundAnimationDuration = 600;
        const factor = (this.drawCtr % backgroundAnimationDuration) / backgroundAnimationDuration;

        if (frameCtr % backgroundAnimationDuration === 1 ) {
            this.backgroundFadeSwitch = !this.backgroundFadeSwitch;
        }

        context.save();

        const gradient = context.createLinearGradient(
            0,
            0,
            CANVAS_WIDTH,
            CANVAS_HEIGHT
        );
        gradient.addColorStop(0, `rgba(60, 120, 170, ${.5 * (1+Math.sin(factor * 2 * Math.PI)/2)})`);
        gradient.addColorStop(1, `rgba(0, 60, 110, ${.5 * (1+Math.cos(factor * 2 * Math.PI)/2)})`);
        
        context.fillStyle = gradient
        context.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        context.restore();
    }
}