class startOfRoundLayer {
    onCountdownOverFn = null;

    time = 0;
    currentTime = 0;
    countDown = '3';
    fnInvoked = true;
    drawCountdown = false
    players = null;
    player = null;

    resetCounter() {
        this.fnInvoked = false;
        this.time = new Date().getTime();
    }

    logic() {
        this.currentTime = new Date().getTime();
        this.drawCountdown = true
        if ((this.currentTime - this.time) > 1000) {
            this.countDown = '2'
            if ((this.currentTime - this.time) > 2000) {
                this.countDown = '1'
                if ((this.currentTime - this.time) > 3000) {
                    if (this.fnInvoked === false) {
                        if (this.onCountdownOverFn !== null) {
                            this.onCountdownOverFn();
                            this.countDown = '3'
                        }
                        this.fnInvoked = true;
                    }
                }
            }
        }    
    }
    
    draw(context){
        const radius = GUI_RADIUS;
        
        const playerIntro = ((3000-(this.currentTime - this.time))/20)
        
        context.save(); 
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.beginPath();
        context.arc(
            CANVAS_WIDTH/2, 
            CANVAS_HEIGHT/2, 
            radius, degToRad(-90), degToRad(-90 + (360 * ((this.currentTime - this.time)/1000))), 
            true);
        context.lineWidth = 10;
        context.strokeStyle = GUI_COLOR;
        context.stroke();
        context.restore();
        
        context.save();
        context.textAlign = 'center';
        context.font = `bold 80px GAME_FONT`;
        context.fillStyle = "white";
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.fillText(
            this.countDown,
            CANVAS_WIDTH / 2,
            (CANVAS_HEIGHT / 2) + 35,
        );
        context.restore();
        
        for (let playerIndex in this.players){
            let player = players[playerIndex];

            context.save();
            context.shadowOffsetY = SHADOW_OFFSET;
            context.shadowColor = COLOR_SHADOW;
            context.fillStyle = player.color;
            context.beginPath();
            context.arc(player.head.x, player.head.y, TAIL_WIDTH + playerIntro, 0, 2 * Math.PI);
            context.fill();    
            context.restore();   
        };
    }
}