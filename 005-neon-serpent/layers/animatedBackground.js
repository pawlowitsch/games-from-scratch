class AnimatedBackground {

    draw(context, frameCtr, scene) {
        context.save();

        let lineHeight = 20;

        let numLines = Math.ceil(3*CANVAS_HEIGHT/lineHeight);

        context.translate(CANVAS_HEIGHT/2, CANVAS_HEIGHT/2);
        context.rotate(degToRad(36));
        context.translate(-CANVAS_HEIGHT/2, -CANVAS_HEIGHT/2);

        for (let i=0;i<numLines;i++) {
            context.fillStyle = "rgba(255, 255, 255, .01)";
            if (i%2 === 0) {
                continue;
            }
            context.fillRect(
                -CANVAS_WIDTH,
                -CANVAS_HEIGHT + (lineHeight*i) + (Math.floor(frameCtr/10)%(lineHeight*2)),
                CANVAS_WIDTH * 3,
                lineHeight
            )
        }
        context.restore();
        /*
            context.save();
            context.globalCompositeOperation = "destination-in";
            context.fillRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
            context.restore();
        */
    }
}