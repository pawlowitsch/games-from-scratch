class PausedLayer {

    draw(context, frameCtr, scene){
        const pauseLayerx = 300;
        const pauseLayery = 400;

        context.save();
        context.fillStyle = "rgba(0,0,0,.2)";
        context.fillRect(0,0, CANVAS_WIDTH, CANVAS_HEIGHT);

        context.fillStyle = GUI_COLOR
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.beginPath();
        context.roundRect((CANVAS_WIDTH/2)-(pauseLayerx/2), (CANVAS_HEIGHT/2.3)-(pauseLayerx/2), pauseLayerx, pauseLayery, [5]);
        context.fill();
        context.restore();
        
        context.save();
        context.font = "30px GAME_FONT";
        context.fillStyle = COLOR_BACKGROUND;
        context.textAlign = "center";
        context.fillText(`PAUSE`, (CANVAS_WIDTH/2), (CANVAS_HEIGHT/2) - 70,);
        context.font = "15px GAME_FONT";
        context.textAlign = "left";
        context.fillText(`(start/enter/esc)   Continue`, (CANVAS_WIDTH/2-120), (CANVAS_HEIGHT/2) + 20,);
        context.fillText(`(select/M/T)  Toggle Fullscreen`, (CANVAS_WIDTH/2-120), (CANVAS_HEIGHT/2) + 60,);
        context.fillText(`(north/P/G)   Back to Title`, (CANVAS_WIDTH/2-120), (CANVAS_HEIGHT/2) + 100,);
        context.restore();
    }     
}
