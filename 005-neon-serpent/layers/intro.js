class IntroLayer {
    backgroundFadeSwitch = false;

    drawCtr = 0;

    image = null;

    constructor() {
        this.image = new Image();
        this.image.src = "./graphics/logo.svg";
    }

    draw(context) {
        this.drawCtr++;

        context.save();
        context.beginPath();
        if (false && this.image) {
            let ratio = this.image.width / this.image.height;
            let targetImageSize = 300;
            context.drawImage(
                this.image,
                0, 0,
                this.image.width, this.image.height,
                (CANVAS_HEIGHT / 2) - (targetImageSize * ratio / 2), (CANVAS_HEIGHT / 3) - (targetImageSize / 2),
                targetImageSize * ratio, targetImageSize
            );
        }
        context.closePath();
        context.restore();
    }
}