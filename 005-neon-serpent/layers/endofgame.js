class endOfGameLayer {
    winner = null;
    paused = false;

    pauseGame() {
        this.paused = !this.paused;
    }

    logic(){
        Controller.singlePress("shooting", "west", (playerIndex, player, controller) => {
            switchScene(SelectColorScreen);
        });
    }

    draw(context, frameCtr, scene) {
        const radius = GUI_RADIUS;
        
        context.save();
        
        context.restore();
        
        context.save();
        context.strokeStyle = this.winner.color;
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.textAlign = 'center';
        context.font = "bold 60px GAME_FONT";
        
        context.fillText(
            'PLAYER ' + (this.winner.playerIndex  + 1) + ' WINS THE GAME!!!',
            CANVAS_WIDTH / 2,
            CANVAS_HEIGHT / 2
            );
        context.restore();
            
    }
}