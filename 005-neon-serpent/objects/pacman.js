class Pacman extends Circle {
    direction = 0;
    speed = PACMAN_SPEED;

    health = PACMAN_INITIAL_HEALTH;

    nextPoint = null;

    aliveAudio = null;
    deathAudio = null;

    audioFiles = {
        pacmanDeath: new Audio('sounds/pacman_death.wav'),
        pacmanAlive: new Audio('sounds/pacman_chomp.wav'),
    }

    constructor(x, y, r) {
        if (!r) r = PACMAN_SIZE;
        super(x, y, r);

        this.deathAudio = this.audioFiles.pacmanDeath;
        this.deathAudio.currentTime = 0;
        this.deathAudio.loop = false;
        
        this.aliveAudio = this.audioFiles.pacmanAlive;
        this.aliveAudio.currentTime = 0;
        this.aliveAudio.loop = true;
    }

    live(scene) {
        this.aliveAudio.play();
    }

    die(scene, hitByBullet) {
        this.aliveAudio.pause();
        
        if (hitByBullet) {
            this.deathAudio.play();
        }
    }

    logic(frameCtr, scene) {
        let halfSize = this.r / 2;

        // next point
        if (this.nextPoint && distance(this, this.nextPoint) <= halfSize) {
            let b = new Bomb();
            b.x = this.nextPoint.x;
            b.y = this.nextPoint.y;
            this.nextPoint = null;
            scene.bombs.push(b);
        }

    
        // walls no good
        if (
            this.x < halfSize || this.x >= CANVAS_WIDTH - halfSize ||
            this.y < halfSize || this.y >= CANVAS_HEIGHT - halfSize
        ) {
            this.nextPoint = { x: CANVAS_WIDTH / 2, y: CANVAS_HEIGHT / 2 };
        }

        if (this.nextPoint === null) {
            this.nextPoint = { x: 0, y: 0 };

            do {
                relocate(this.nextPoint, false, scene.generateCollidableObjectArray());
            } while (
                this.nextPoint.x < halfSize || this.nextPoint.x >= CANVAS_WIDTH - halfSize ||
                this.nextPoint.y < halfSize || this.nextPoint.y >= CANVAS_HEIGHT - halfSize
            );
        }

        let steeringFrames = 10;
        let deltaDirection = (this.direction - getAngleDegrees(this.nextPoint.x, this.nextPoint.y, this.x, this.y)) / steeringFrames;
        this.direction -= deltaDirection;

        this.direction = this.direction % 360;

        this.x += this.speed * -1 * Math.sin(degToRad(this.direction));
        this.y += this.speed * Math.cos(degToRad(this.direction));
    }

    draw(context, frameCtr, scene) {
        context.save();
        const mouthFrames = 75;
        const mouthMaxOpenAngle = 45;
        const mouthOpenAngle = 1 + (mouthMaxOpenAngle * Math.sin(Math.PI * ((frameCtr%mouthFrames) / mouthFrames)));

        if (this.nextPoint) {
            context.save();
            context.beginPath();
            context.strokeStyle = "rgba(255,255,255, .6";
            context.lineWidth = 4;
            context.setLineDash([2, 5]);
            context.arc(this.nextPoint.x, this.nextPoint.y, PACMAN_NEXT_POINT_SIZE, 0, 360);
            context.stroke();
            context.closePath();
            context.restore();
        }

        // draw healthbar
        let healthbarWidth = 20;
        context.lineWidth = 5;
        context.strokeStyle = "black";
        context.beginPath();
        context.moveTo(
            this.x + (this.r / 2),
            this.y - (this.r / 2),
        );
        context.lineTo(
            this.x + (this.r / 2) + healthbarWidth,
            this.y - (this.r / 2),
        );
        context.stroke();
        context.closePath();

        context.strokeStyle = "white";
        context.beginPath();
        context.moveTo(
            this.x + (this.r / 2),
            this.y - (this.r / 2),
        );
        context.lineTo(
            this.x + (this.r / 2) + ((this.health / PACMAN_INITIAL_HEALTH) * healthbarWidth),
            this.y - (this.r / 2),
        );
        context.stroke();
        context.closePath();

        // draw pacman
        context.beginPath();

        context.fillStyle = "yellow";
        context.lineWidth = 2;
        context.strokeStyle = "black";

        context.translate(this.x, this.y)
        context.rotate(degToRad(90 + this.direction));

        context.lineTo(0, 0);
        context.arc(
            0, 0,
            this.r / 2,
            degToRad(-mouthOpenAngle / 2),
            degToRad(mouthOpenAngle),
            true
        );

        context.lineTo(0, 0);

        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;

        context.fill();
        // context.stroke();

        context.closePath();

        context.restore();
    }
}