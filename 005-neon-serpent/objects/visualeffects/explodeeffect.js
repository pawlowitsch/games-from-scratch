class ExplodeEffect extends VisualEffect {
    t = 0;

    constructor(x, y) {
        super(x, y);
    }

    completed() {
        return this.t > EXPLODE_EFFECT_DURATION;
    }

    logic(frameCtr, scene) {
        this.t++;        
    }

    drawSmoke(x, y, r) {
        const smokeRadius = 60 + r; // base size of smoke particle with r for variation
        const perc = this.t / EXPLODE_EFFECT_DURATION;
        const targetImageSize = (smokeRadius * (-1*perc)) + smokeRadius;
        context.save(); 
        context.globalAlpha = 1;
        context.beginPath();
        context.translate(x, y);
        context.rotate(perc*4) // rotation speed         
        context.drawImage(
            ImageService.images.smoke1,
            - (targetImageSize / 2), - (targetImageSize / 2),
            targetImageSize, targetImageSize
        );
        context.closePath();
        context.restore();
    }

    draw(context, frameCtr, scene) {

        const smokeDist = 40 * ((this.t + 1) / EXPLODE_EFFECT_DURATION); // how far smoke particles travles to the outside
        
        this.drawSmoke(this.x + smokeDist, this.y, 5);
        this.drawSmoke(this.x - smokeDist, this.y, -8);
        this.drawSmoke(this.x, this.y + smokeDist, 5);
        this.drawSmoke(this.x, this.y - smokeDist, 5);                
        this.drawSmoke(this.x + smokeDist, this.y - smokeDist, -10);                
        this.drawSmoke(this.x - smokeDist, this.y - smokeDist, 10);                
    }
}
