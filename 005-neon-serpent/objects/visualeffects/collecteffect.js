class CollectEffect extends VisualEffect {
    v = 0;
    t = 0;

    playerIndex = 0;
    image = null;

    constructor(x, y, playerIndex) {
        super(x, y);

        this.image = new Image();
        this.image.src = "./graphics/effects/pop.svg"

        this.v = degToRad(players[playerIndex].direction);       
    }

    completed() {
        return this.t > EFFECT_DURATION_POP;
    }

    logic(frameCtr, scene) {
        this.t++;
    }

    draw(context, frameCtr, scene) {
        const perc = this.t / EFFECT_DURATION_POP;
        context.save();
        context.beginPath();
        if (this.image) {
            let targetImageSize = 50 + (this.t/2);
            context.globalAlpha = (1 * (-1*perc)) + 1.1;
            context.translate(this.x, this.y);
            context.rotate(this.v);            
            context.drawImage(
                this.image,
                - (targetImageSize / 2), - (targetImageSize / 2),
                targetImageSize, targetImageSize
            );
        }
        context.closePath();
        context.restore();
    }
}