class Player {
    playerIndex = null;
    speed = SPEED;
    bucket = 0;
    wins = 0;
    alive = true;
    length = START_LENGTH;
    lastLength = 0;
    direction = 90;
    head = new PlayerHead(100, 0, PLAYER_RADIUS, this);
    tail = [];
    color = this[0];
    tailColor = this[this.length];
    drawDashed = false;
    drawOnlyPlayer = false;
    lastMultiplikator = 1;

    onetimeItem = null;
    permanentItems = [];

    static clonePlayer(p) {
        const clone = new Player();

        clone.playerIndex = p.playerIndex;
        clone.speed = p.speed;
        clone.bucket = p.bucket;
        clone.wins = p.wins;
        clone.alive = p.alive;
        clone.length = p.length;
        clone.direction = p.direction;
        clone.head = p.head;
        clone.tail = p.tail;
        clone.color = p.color;
        clone.tailColor = p.tailColor;
        clone.drawDashed = p.drawDashed;
        clone.lastLength = p.lastLength;
        clone.lastMultiplikator = p.lastMultiplikator;

        clone.onetimeItem = p.onetimeItem;
        clone.permanentItems = [...p.permanentItems];

        return clone;
    }

    logic(frameCtr, scene) {
        // add tailitem at the beginning of tail
        if (frameCtr % TAIL_FRAME_CALC === 0) {
            const newTailElement = new PlayerTail(this.head.x, this.head.y, this.head.r, this);
            newTailElement.tooCloseToHead = true;

            this.tail.unshift(newTailElement);

            // mark all tail items after the second as not tooCloseToHead,
            // so that they are considererd in collision detection
            if (this.tail[5]) this.tail[5].tooCloseToHead = false;
        }

        // delete last tail item
        if (this.tail.length > this.length && frameCtr % 2 === 0) {
            this.tail.pop();
        }

        if (this.alive === false) return;

        this.head.x += (scene.gameSpeedIncrease + this.speed) * Math.cos(degToRad(this.direction));
        this.head.y += (scene.gameSpeedIncrease + this.speed) * Math.sin(degToRad(this.direction));

        // garbage collection of permanent items
        this.permanentItems = this.permanentItems.filter(pi => pi.isDestroyed === false);

        for (let permanentItem of this.permanentItems) {
            permanentItem.logic(frameCtr, scene);
        }
    }

    drawTail(context, frameCtr, scene, color, dashed=false, shadow=true) {
        context.save();

        if (dashed) {
            context.setLineDash([5,15]);
        }

        context.beginPath();
        context.strokeStyle = color;
        context.lineWidth = TAIL_WIDTH;
        context.lineCap = "round";
        context.moveTo(this.head.x, this.head.y);
        for (let i=0;i<this.tail.length-1;i++) {
            let tailElement1 = this.tail[i+1];

            if (tailElement1.isHole === true) {
                context.moveTo(tailElement1.x, tailElement1.y,);
                continue;
            }
            
            context.lineTo(tailElement1.x, tailElement1.y,);
        }
        if (shadow) {
            context.shadowOffsetY = SHADOW_OFFSET;
            context.shadowColor = COLOR_SHADOW;
        }
        context.stroke();
        context.closePath();

        context.restore();
    }

    drawHead(context, frameCtr, scene) {
        context.save();
        context.beginPath();
        context.arc(this.head.x, this.head.y, PLAYER_RADIUS, 0, 2 * Math.PI, false);
        context.fillStyle = this.color;
        context.fill();
        context.closePath();
        context.restore();
    }

    drawBulletCounter(context, frameCtr, scene) {
        context.save();
        let numberOfBullets = ((this.length -START_LENGTH) / INCREASE_LENGTH_VALUE);
        context.beginPath();
        context.textAlign = 'left'; 
        context.font = "20px GAME_FONT";
        context.fillStyle = 'rgba(255, 255, 255, .9)';
        context.fillText(
            numberOfBullets,
            this.head.x + 10,
            this.head.y
        );
        context.closePath();
        context.restore();
    }

    drawDebugInfo(context, frameCtr, scene) {
        return;
        
        context.save();
        context.beginPath();
        context.textAlign = 'left'; 
        context.font = "20px GAME_FONT";
        context.fillStyle = 'rgba(255, 255, 255, .25)';
        context.fillText(
            `debug: ${this.tail.length}`,
            this.head.x + 10,
            this.head.y + 30
        );
        context.closePath();
        context.restore();
    }

    drawEquipetOnetimeItem(context, frameCtr, scene) {
        context.save();
        context.beginPath();
        context.arc(
            this.head.x - 10,
            this.head.y,
            PLAYER_RADIUS/2,
            0,
            2 * Math.PI,
            false
        );
        context.fillStyle = "red";
        context.fill();
        context.closePath();
        context.restore();

        this.onetimeItem.draw(context, frameCtr, scene);
    }

    draw(context, frameCtr, scene) {
        if (this.alive === false) return;

        // ---- tail
        let color = this.tailColor;
        if (this.drawDashed) {
            color = "white";
        }
        this.drawTail(context, frameCtr, scene, color);

        if (this.drawDashed) {
            this.drawTail(context, frameCtr, scene, this.tailColor, true, false);
        }
    
        // ---- head
        this.drawHead(context, frameCtr, scene);

        // ---- additional player stuff
        if (this.drawOnlyPlayer === false) {
            this.drawBulletCounter(context, frameCtr, scene)

            if (this.onetimeItem !== null) {
                this.drawEquipetOnetimeItem(context, frameCtr, scene);
            }

            for (let permanentItem of this.permanentItems) {
                permanentItem.draw(context, frameCtr, scene);
            }
        }

        this.drawDebugInfo(context, frameCtr, scene);
    }
}