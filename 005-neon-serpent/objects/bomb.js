class Bomb extends Circle {
    t = 0;
    exploded = false;
    
    image = null;

    constructor(x, y, r) {
        if (!r) r = 20;

        super (x, y, r);

        this.image = new Image();
        this.image.src = "./graphics/bomb.svg";
    }

    logic(frameCtr, scene) {

    }

    draw(context, frameCtr, scene) {
        context.save();
        context.beginPath();
        if (this.image) {
            let ratio = this.image.width / this.image.height;
            let targetImageSize = 40;
            context.drawImage(
                this.image,
                0, 0,
                this.image.width, this.image.height,
                this.x - (targetImageSize * ratio / 2), this.y - (targetImageSize / 2),
                targetImageSize * ratio, targetImageSize
            );
        }
        context.closePath();
        context.restore();
    }

    explode(playerIndex, player, scene) {
        this.exploded = true;
        SoundService.playSound("boom");
    }
}