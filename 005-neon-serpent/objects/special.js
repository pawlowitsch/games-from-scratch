class Special extends Circle {

    // TODO: r und constructor

    static types = {
        SQUARED: "BE SQUARE",
        //REVERSED: "GO BACKWARDS!",
        //SMALLAGAIN: "BE small",
        //HAVEHOLES: "BE INCOMPLETE",
        DISTORTED: "SHAKE IT UP",
        //BARREL: "ROLL OUT",
        //CAREFUL: "BE CAREFUL",
        PACMAN: "PACMAN",
        //BEBIG: "BE BIG",
    };

    t = 0;

    payload = null;

    duration = SPECIAL_DURATION;

    isCollected = false;

    image = null;
    imageAnnouncment = null;

    type = null;

    announcmentDuration = ANNOUNCMENT_TIME;

    announcmentIsActive = true;

    constructor(x, y, r) {
        if (!r) r = SPECIAL_RADIUS;

        super(x, y, r);
        
        this.image = new Image();
        this.image.src = "./graphics/special.svg";

        this.selectType();
    }

    selectType(skipPacman) {
        let typeKeys = Object.keys(Special.types);
        if (skipPacman) {
            typeKeys = Object.keys(Special.types).filter(v => v != "PACMAN");
        }
        let typeIdx = typeKeys[Math.min(Math.floor(Math.random() * typeKeys.length), typeKeys.length - 1)];
        this.type = Special.types[typeIdx];

        this.imageAnnouncment = new Image();
        this.imageAnnouncment.src = `./graphics/announcements/${typeIdx.toLowerCase()}.svg`;
    }

    collected(playerIndex, player, scene) {
        if (this.type === Special.types.PACMAN && scene.pacman) {
            this.selectType(true);
        }

        this.isCollected = true;
    }

    announcmentOver(scene) {
        this.t = 0;
        this.announcmentIsActive = false;

        if (this.type === Special.types.REVERSED) {
            this.reversePlayers(scene);
        }

        if (this.type === Special.types.SMALLAGAIN) {
            this.makeAllSmallAgain(scene);
        }

        if (this.type === Special.types.HAVEHOLES) {
            this.makeHoles(scene);
        }

        if (this.type === Special.types.CAREFUL) {
            this.placeBombs(scene);
        }

        if (this.type === Special.types.PACMAN) {
            this.pacman(scene);
        }

        if (this.type === Special.types.BEBIG) {
            this.bebig(scene);
        }
    }

    bebig(scene) {
        this.t = Number.MAX_SAFE_INTEGER;

        for (let player of players) {
            player.length = START_LENGTH + (40 * LENGTH_INCREASE);
        }

    }

    pacman(scene) {
        this.t = Number.MAX_SAFE_INTEGER;

        if (scene.pacman) return;

        scene.pacman = new Pacman();
        relocate(scene.pacman, false, scene.generateCollidableObjectArray());
        scene.pacman.live(scene);
    }

    placeBombs(scene) {
        this.t = Number.MAX_SAFE_INTEGER;
        for (let i=0;i<NUMBER_OF_BOMBS_PLACED;i++) {
            let b = new Bomb();
            relocate(b, false, scene.generateCollidableObjectArray());
            scene.bombs.push(b);
        }
    }

    makeAllSmallAgain(scene) {
        this.t = Number.MAX_SAFE_INTEGER;

        for (let player of players) {
            player.length = START_LENGTH;
        }
    }

    makeHoles(scene) {
        this.t = Number.MAX_SAFE_INTEGER;

        for (let playerIndex in players) {
            const player = players[playerIndex];
            if (player.length <= START_LENGTH + LENGTH_INCREASE) continue;

            let punshHole = false;
            let ctr = 0;
            let distBetweenHoles = 0;
            for (let i = 0; i < player.length - 1; i++) {
                if (!player.tail[i]) continue;

                if (distBetweenHoles++ <= HOLE_MIN_DIST) continue;

                if (punshHole === false && Math.random() > HOLE_THRESHOLD) {
                    punshHole = true;

                    let e = new Effect();
                    e.x = player.tail[i].x;
                    e.y = player.tail[i].y;
                    e.playerIndex = playerIndex
                    scene.effects.push(e);
                }

                if (punshHole === true) {
                    player.tail[i].isHole = true;

                    if (ctr++ > HOLE_LENGTH) {
                        ctr = 0;
                        distBetweenHoles = 0;
                        punshHole = false;
                    }
                }
            }
        }
    }

    timedOut() {
        if (this.type === Special.types.REVERSED) {
            this.reversePlayers();
        }
    }

    reversePlayers(scene) {
        for (const p of players) {
            p.tail.reverse();
            p.tail.push(p.head);
            p.head = p.tail.shift();
            p.direction = getAngleDegrees(
                p.tail[5].x, p.tail[5].y,
                p.head.x, p.head.y,
            ) - 90;
        }
    }

    logic(frameCtr, scene) {
        if (this.isCollected === false) return;

        this.t++;

        if (this.announcmentIsActive) return;

        if (this.type === Special.types.SQUARED) {
            return this.squaredLogic(frameCtr, scene);
        }
    }

    squaredLogic() {
        Controller.singlePress("turn left", "left", (playerIndex, player, controller) => {
            player.direction -= 90;
        });

        Controller.singlePress("turn right", "right", (playerIndex, player, controller) => {
            player.direction += 90;
        });
    }

    draw(context, frameCtr, scene) {
        // draw the collectable item
        if (this.isCollected === false) {
            context.save();

            context.beginPath();
            if (this.image) {
                let ratio = this.image.width/this.image.height;
                let targetImageSize = this.r;
                context.drawImage(
                    this.image,
                    0, 0,
                    this.image.width, this.image.height,
                    this.x - (targetImageSize * ratio / 2), this.y - (targetImageSize / 2),
                    targetImageSize * ratio, targetImageSize
                );

                // ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
            } else {
                context.fillStyle = "red";
                context.shadowOffsetY = SHADOW_OFFSET;
                context.shadowColor = COLOR_SHADOW;
                context.arc(this.x, this.y, this.r, 0, 2 * Math.PI, false);
                context.fill();
            }
            context.closePath();

            context.restore();
        }

        // draw the announcment text for the item
        if (this.announcmentIsActive === true && this.isCollected === true) {
            //const announcment = `TIME TO ${this.type}`;

            if (this.imageAnnouncment) {
                let ratio = this.imageAnnouncment.width / this.imageAnnouncment.height;
                let targetImageSize = 200;
                context.drawImage(
                    this.imageAnnouncment,
                    0, 0,
                    this.imageAnnouncment.width, this.imageAnnouncment.height,
                    (CANVAS_WIDTH / 2) - (targetImageSize * ratio / 2), (CANVAS_HEIGHT / 2) - (targetImageSize / 2),
                    targetImageSize * ratio, targetImageSize
                );

                /* // Announcment Text
                let right = CANVAS_WIDTH;
                let textWidth = (context.measureText(announcment).width) * 3;
                right -= (CANVAS_HEIGHT + (textWidth)) * (this.t / this.announcmentDuration);

                context.save();
                context.fillStyle = "rgba(255,255,255,1)";
                context.textAlign = "left";
                context.font = "italic 30px Monospace";
                context.fillText(
                    announcment,
                    right, CANVAS_HEIGHT / 1.62
                );*/
            }

            context.fillStyle = "rgba(0,0,0,.3)";
            context.fillRect(1, CANVAS_WIDTH / 1.62, CANVAS_HEIGHT - 2, 30);
            context.restore();
        }

        // draw the countdown at the players position
        if (this.announcmentIsActive === false && this.isCollected === true) {
            const circleRadius = 15;
            const circleWidth = 4;
            const circleOffset = 0;
            for (let player of players.filter(p => p.alive)) {
                context.save();
                context.beginPath();
                context.strokeStyle = "white";
                context.lineWidth = circleWidth;
                context.arc(
                    player.head.x + circleOffset, player.head.y - circleOffset,
                    circleRadius,
                    degToRad(-90), degToRad(-90 + (360 * (this.t / this.duration))),
                    true
                );
                context.stroke();
                context.closePath();
                context.restore();
            }
        }
    }
}