class Object2D {
    static globalMaxId = 0;

    id = ""+Object2D.globalMaxId++;

    x = null;
    y = null;

    constructor(x, y) {
        if ( notset(x) || notset(y)) throw new Error("Missing X or Y for Object2D constructor");

        this.x = x;
        this.y = y;
    }
}

class Circle extends Object2D {
    r = null;

    constructor(x, y, r) {
        if ( notset(r)) throw new Error("Missing R for Circle constructor");

        super(x, y);

        this.r = r;
    }
}

class Rect extends Object2D  {
    w = null;
    h = null;

    constructor(x, y, w, h) {
        if ( notset(w) || notset(h) ) throw new Error("Missing W or H for Rect constructor");

        super(x, y);

        this.w = w;
        this.h = h;
    }
}

class PlayerPart extends Circle {
    relatedPlayer;

    constructor(x, y, r, relatedPlayer) {
        if ( notset(relatedPlayer) ) throw new Error("Missing RELATEDPLAYER for PlayerPart constructor");

        super(x, y, r);

        this.relatedPlayer = relatedPlayer;
    }
}

class PlayerHead extends PlayerPart {

}

class PlayerTail extends PlayerPart {
    tooCloseToHead = false;
}