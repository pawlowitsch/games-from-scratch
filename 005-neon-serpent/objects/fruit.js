class Fruit extends Circle{
    image = null;
    
    constructor(x, y, r) {
        if (!r) r = FRUIT_RADIUS;

        super(x, y, r);

        this.imageAmmo = new Image();
        this.imageAmmo.src = "./graphics/ammo.svg";
    }

    logic(frameCtr, scene) {

    }

    draw(context, frameCtr, scene) {
        context.save();
        context.beginPath();
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.drawImage(
            this.imageAmmo,
            0, 0,
            this.imageAmmo.width, this.imageAmmo.height,
            this.x-(FRUIT_RADIUS/2), this.y-(FRUIT_RADIUS/2),
            FRUIT_RADIUS, FRUIT_RADIUS,
        );
        context.closePath();
        context.restore();
    }
}