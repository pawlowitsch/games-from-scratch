class RocketItem extends ActivePlayerItem {
    targetPlayer = null;

    timeUntilCanExplode = TRIGGERED_PLAYERITEM_ACTIVATION_TIME;

    direction = null;

    angle = 0;

    constructor(x, y, r) {
        if (!r) r = SPECIAL_RADIUS;
        super(x, y, r);
    }

    collectedBy(player) {
        super.collectedBy(player);

        this.r = TRIGGERED_PLAYER_ITEM_RADIUS;
    }

    use() {
        super.use();

        this.targetPlayer = getNearestTargetPlayer(
            this.playerThatCollectedItem.head.x,
            this.playerThatCollectedItem.head.y,
            [this.playerThatCollectedItem]
        );
    }

    logicCollectedAndActive(frameCtr, scene) {
        super.logicCollectedAndActive(frameCtr, scene);

        if (this.targetPlayer === null) return;

        // count down until the bomb can explode
        // this is a security measurement to not explode directly after
        // shooting the bomb
        if (this.canCollide === false && this.timeUntilCanExplode-- > 0) {
            if (this.timeUntilCanExplode <= 0) {
                this.canCollide = true;
            }
        }

        const targetAngle = getAngleDegrees(
            this.x,
            this.y,
            this.targetPlayer.head.x,
            this.targetPlayer.head.y
        );

        this.direction = targetAngle - 90;

        const travelSpeed = 1;

        this.x += travelSpeed * Math.cos(degToRad(this.direction));
        this.y += travelSpeed * Math.sin(degToRad(this.direction));
    }

    drawCollectedAndInactive(context, frameCtr, scene) {
        const targetPlayer =  getNearestTargetPlayer(
            this.playerThatCollectedItem.head.x,
            this.playerThatCollectedItem.head.y,
            [ this.playerThatCollectedItem ]
        );

        // single player case
        if (targetPlayer !== null) {
            let crossHairRadius = 30;
            const gradient = context.createLinearGradient(this.playerThatCollectedItem.head.x, this.playerThatCollectedItem.head.y, targetPlayer.head.x, targetPlayer.head.y);

            context.save();
            gradient.addColorStop(0, 'rgba(255, 255, 255, 0)');
            gradient.addColorStop(0.5, "red");

            context.strokeStyle = gradient;
            context.lineWidth = 1;

            context.beginPath();
            context.moveTo(this.playerThatCollectedItem.head.x, this.playerThatCollectedItem.head.y);
            context.lineTo(targetPlayer.head.x, targetPlayer.head.y);
            context.stroke();
            context.closePath();
            context.restore();

            context.save();
            context.beginPath();
            context.drawImage(
                ImageService.images.crosshair,
                0,
                0,
                ImageService.images.crosshair.width,
                ImageService.images.crosshair.height,
                targetPlayer.head.x - (crossHairRadius/2),
                targetPlayer.head.y - (crossHairRadius/2),
                crossHairRadius,
                crossHairRadius,
            );
            context.restore();
        }
    }

    drawCollectedAndActive(context, frameCtr, scene) {
        // single player case
        if (this.targetPlayer === null) return;

        context.save();
        context.beginPath();
        if (ImageService.images.torpedo) {
            let targetImageSize = 60;
            context.translate(this.x, this.y);
            context.rotate(degToRad(this.angle));
            context.drawImage(
                ImageService.images.torpedo,
                - (targetImageSize / 2),
                - (targetImageSize / 2),
                targetImageSize,
                targetImageSize
            );
        }
        context.restore();
        
        this.angle++;

        const gradient = context.createLinearGradient(this.x, this.y, this.targetPlayer.head.x, this.targetPlayer.head.y);
        gradient.addColorStop(0, 'rgba(255, 255, 255, 0)');
        gradient.addColorStop(0.5, "red");

        let crossHairRadius = 40;
        context.save();
        
        context.strokeStyle = gradient;
        context.lineWidth = 1;

        context.beginPath();

        context.moveTo(this.x, this.y);
        context.lineTo(this.targetPlayer.head.x, this.targetPlayer.head.y);

        context.stroke();
        context.closePath();
        context.restore();

        context.save();
        context.beginPath();
        context.drawImage(
            ImageService.images.crosshair,
            0,
            0,
            ImageService.images.crosshair.width,
            ImageService.images.crosshair.height,
            this.targetPlayer.head.x - (crossHairRadius/2),
            this.targetPlayer.head.y - (crossHairRadius/2),
            crossHairRadius,
            crossHairRadius,
        );
        context.restore();
    }
}