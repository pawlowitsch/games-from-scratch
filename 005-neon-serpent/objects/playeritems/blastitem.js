class BlastItem extends AreaEffectPlayerItem {
    static EFFECT_TIME = 80;
    static RADIUS = 100;

    t = 0;
    drawAngle = 0;

    constructor(x, y, r) {
        if (!r) r = SPECIAL_RADIUS;
        super(x, y, r);
    }

    collectedBy(player) {
        super.collectedBy(player);
        this.r = BlastItem.RADIUS;
    }

    use() {
        super.use();

        this.canCollide = true;
    }

    logicCollectedAndActive(frameCtr, scene) {
        super.logicCollectedAndActive(frameCtr, scene);

        if (this.t++ > BlastItem.EFFECT_TIME) return this.destroy();

        this.x = this.playerThatCollectedItem.head.x;
        this.y = this.playerThatCollectedItem.head.y;
    }

    drawCollectedAndInactive(context, frameCtr, scene) {
        super.drawCollectedAndInactive(context, frameCtr, scene);

        this.drawAngle -= .1;

        context.save();
        context.beginPath();

        context.translate(
            this.playerThatCollectedItem.head.x,
            this.playerThatCollectedItem.head.y
        );
        context.rotate(this.drawAngle);

        const gradient = context.createConicGradient(0, 0, 0);
        gradient.addColorStop(0, 'rgba(150, 150, 255, .3)'); 
        gradient.addColorStop(.5, 'rgba(150, 150, 255, 0)'); 

        context.lineWidth = 10;
        context.strokeStyle = gradient;
        context.lineCap = "round";

        context.arc(
            0,
            0,
            this.r,
            0,
            Math.PI * 2
        );
        context.stroke();

        context.closePath();
        context.restore();
    }

    drawCollectedAndActive(context, frameCtr, scene) {
        super.drawCollectedAndInactive(context, frameCtr, scene);

        const perc = this.t / BlastItem.EFFECT_TIME;

        context.save();
        context.beginPath();

        context.translate(
            this.playerThatCollectedItem.head.x,
            this.playerThatCollectedItem.head.y
        );
        context.rotate(this.drawAngle);

        const gradient = context.createRadialGradient(
            0, 0, 0,
            0, 0, this.r
        );
        gradient.addColorStop(Math.max(0, perc-.5), 'rgba(150, 150, 255, 0)'); 
        gradient.addColorStop(.8, 'rgba(150, 150, 255, .6)'); 
        gradient.addColorStop(1, 'rgba(150, 150, 255, 0)'); 

        context.fillStyle = gradient;

        context.arc(
            0,
            0,
            this.r*perc,
            0,
            Math.PI * 2
        );
        context.fill();

        context.closePath();
        context.restore();
    }
}