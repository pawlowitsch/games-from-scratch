class SlowdownItem extends PassivePlayerItem {
    t = 0;

    constructor(x, y, r) {
        if (!r) r = SPECIAL_RADIUS;
        super(x, y, r);
    }

    logicCollectedAndActive(frameCtr, scene) {
        super.logicCollectedAndActive(frameCtr, scene);

        if (this.t++ >= 1000) {
            this.isDestroyed = true;
            this.playerThatCollectedItem.speed = SPEED;
            return;
        }
        
        this.playerThatCollectedItem.speed = 0.4 * SPEED;
    }

    drawCollectedAndActive(context, frameCtr, scene) {
        context.save();
        context.beginPath();
        context.textAlign = 'left'; 
        context.font = "20px GAME_FONT";
        context.fillStyle = 'rgba(255, 255, 255, .25)';
        context.fillText(
            `SLOW`,
            this.playerThatCollectedItem.head.x + 10,
            this.playerThatCollectedItem.head.y + 30
        );
        context.closePath();
        context.restore();
    }
}