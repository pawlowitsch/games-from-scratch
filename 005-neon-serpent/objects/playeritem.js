/**
 * player items have a logic cycle
 * 
 * player items have a draw cycle
 * 
 * player items can be collected
 * 
 * player items have different states
 * 
 * - state "isCollected === false": player item is a collectable object on playfield
 * - state "isCollected === true": player item has been collected by a player
 * 
 * - state "isCollected and isActive === false":
 *          this state should only be available for ActivePlayerItem's
 *          and represents the state of "collected but not used"
 * 
 * - state "isCollected and isActive === true":
 *          the item has been collected and is in use.
 *          either as something active like a rocket
 *          or like something passive like a shield
 * 
 * playeritems are drawn differently in each state.
 * playeritems logic is also differently in each state.
 */

class PlayerItem extends Circle {
    isCollected = false;
    isActive = false;
    isDestroyed = false;
    playerThatCollectedItem = null;

    collectedBy(player) {
        this.isCollected = true;
        this.playerThatCollectedItem = player;
    }

    destroy() {
        this.isDestroyed = true;
    }

    logicNotCollected(frameCtr, scene) {

    }

    logicCollectedAndInactive(frameCtr, scene) {

    }

    logicCollectedAndActive(frameCtr, scene) {

    }

    logic(frameCtr, scene) {
        if (this.isDestroyed === true) return;

        if (this.isCollected === false) return this.logicNotCollected(frameCtr, scene);
        if (this.isActive === false) return this.logicCollectedAndInactive(frameCtr, scene);
        if (this.isActive === true) return this.logicCollectedAndActive(frameCtr, scene);
    }

    drawNotCollected(context, frameCtr, scene) {
        context.save();
        context.beginPath();

        if (ImageService.images.special) {
            let ratio = ImageService.images.special.width/ImageService.images.special.height;
            let targetImageSize = this.r;
            
            context.drawImage(
                ImageService.images.special,
                0,
                0,
                ImageService.images.special.width,
                ImageService.images.special.height,
                this.x - (targetImageSize * ratio / 2),
                this.y - (targetImageSize / 2),
                targetImageSize * ratio, targetImageSize
            );
        }

        context.closePath();
        context.restore();
    }

    drawCollectedAndInactive(context, frameCtr, scene) {

    }

    drawCollectedAndActive(context, frameCtr, scene) {

    }

    draw(context, frameCtr, scene) {
        if (this.isDestroyed === true) return;

        if (this.isCollected === false) return this.drawNotCollected(context, frameCtr, scene);
        if (this.isActive === false) return this.drawCollectedAndInactive(context, frameCtr, scene);
        if (this.isActive === true) return this.drawCollectedAndActive(context, frameCtr, scene);
    }
}

class ActivePlayerItem extends PlayerItem {
    canCollide = false;

    collectedBy(player) {
        super.collectedBy(player);

        player.onetimeItem = this;
    }

    use() {
        this.isActive = true;
        this.playerThatCollectedItem.onetimeItem = null;
    }

    logic(frameCtr, scene) {
        super.logic(frameCtr, scene);
    }
}

class PassivePlayerItem extends PlayerItem {
    collectedBy(player) {
        super.collectedBy(player);

        this.isActive = true;
        player.permanentItems.push(this);
    }
}

class AreaEffectPlayerItem extends ActivePlayerItem {}