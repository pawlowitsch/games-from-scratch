class Block extends Rect {
    blockSize = BLOCK_SIZE;

    health = MAX_BLOCK_HEALTH;

    constructor(x, y, w, h) {
        if (!w) w = BLOCK_SIZE;
        if (!h) h = BLOCK_SIZE;

        super(x, y, w, h);
    }

    logic(frameCtr, scene) {

    }

    draw(context, frameCtr, scene) {
        context.save();
        const visibility = (this.health/MAX_BLOCK_HEALTH);
        context.fillStyle = `rgba(255, 255, 255, ${visibility})`;
        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor = COLOR_SHADOW;
        context.fillRect(
            this.x-(this.blockSize/2),
            this.y-(this.blockSize/2),
            this.blockSize, 
            this.blockSize);
        context.restore();
        
        if(this.health < MAX_BLOCK_HEALTH){
            context.save();
            context.translate(this.x, this.y);
                context.drawImage(
                    ImageService.images.wallcrack,
                    - (this.blockSize / 2),
                    - (this.blockSize / 2),
                    this.blockSize,
                    this.blockSize
                );
            context.restore();
        }
    }
}