class Bullet extends Circle {
    t = 0;

    playerIndex;
    direction;

    constructor(x, y, r, playerIndex, direction) {
        if (!r) r = BULLET_SIZE;

        super(x, y, r);

        this.playerIndex = playerIndex;
        this.direction = direction;
    }

    logic(frameCtr, scene) {
        this.t++;
        
        this.x += BULLET_SPEED * Math.cos(degToRad(this.direction));
        this.y += BULLET_SPEED * Math.sin(degToRad(this.direction));
    }

    draw(context, frameCtr, scene) {
        context.save();
        context.beginPath();

        context.fillStyle = 'white';

        context.arc(this.x, this.y, this.r, 0, 2 * Math.PI, false);

        context.shadowOffsetY = SHADOW_OFFSET;
        context.shadowColor =  COLOR_SHADOW;
        context.fill();   
        context.closePath();
        context.restore();
    }
}