class VisualEffect extends Object2D {

    /**
     * 
     * @returns Boolean true: Effect is over and can be garbagecollected by gamescene; false: onging
     */
    completed() {
        return true;
    }
}