let players = [];
let controls = [];

const addController = () => {
    controls.push( new Controller() );
};

const addPlayer = () => {
    let p = new Player();
    p.playerIndex = players.length;
    p.alive = false
    players.push( p );
};