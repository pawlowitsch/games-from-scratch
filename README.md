# games from scratch

This is a collection of browser games, developed without any framework or library.

## why
Why not? But srsly, mostly because of the fun and in order to see how fast one can do those games.
In addition its nice to explain to others step by step how you would do this.

## list of games
- [Flappy Birds](./001-flappy-birds/index.html)
- [Nibbles](./002-nibbles/index.html)
- [Two Player Tetris](./003-two-player-tetris/index.html)
- [Arkanoid (Not working yet)](./004-arkanoid/index.html)
- [Neon Serpent](./005-neon-serpent/index.html)]
